-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2017 a las 22:44:09
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `obsvirtual`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `card`
--

CREATE TABLE `card` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `card`
--

INSERT INTO `card` (`id`, `des`, `ts`) VALUES
(1, 'Tarjeta Amarilla', '2017-03-30 15:40:30'),
(2, 'Tarjeta Roja', '2017-03-30 15:40:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fault`
--

CREATE TABLE `fault` (
  `id` int(2) NOT NULL,
  `cod` varchar(250) DEFAULT NULL,
  `des` varchar(250) DEFAULT NULL,
  `id_fault_type` int(1) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fault`
--

INSERT INTO `fault` (`id`, `cod`, `des`, `id_fault_type`, `ts`) VALUES
(1, '22.1', 'Los retardos e inasistencia a clases sin justificación.', 1, '2017-04-06 16:17:29'),
(2, '22.2', 'El no portar el uniforme y su uso de manera inadecuada', 1, '2017-04-06 16:17:29'),
(3, '22.3', 'Observaciones o llamados de atención por parte de las personas\r\nexternas sobre comportamientos anti-convivenciales observados en los\r\nestudiantes', 1, '2017-04-06 16:17:29'),
(4, '22.4', 'El incumplimiento con las tareas y trabajos asignados', 1, '2017-04-06 16:17:29'),
(5, '22.5', 'La charla constante en la clase o en actos comunitarios', 1, '2017-04-06 16:17:29'),
(6, '22.6', 'Indelicadeza de forma verbal o física hecha a los compañeros,\r\nprofesores, empleados y demás personas de la comunidad educativa', 1, '2017-04-06 16:17:29'),
(7, '22.7', 'El juego brusco', 1, '2017-04-06 16:17:29'),
(8, '22.8', 'La perturbación de clases con dichos o hechos inoportunos', 1, '2017-04-06 16:17:29'),
(9, '22.9', 'Deficiente presentación personal e higiene corporal, cortes inadecuados,\r\ntintes diversos, peinados extraños y demás formas no clásicas', 1, '2017-04-06 16:17:29'),
(10, '22.10', 'Permanecer fuera de los salones indicados, en horas de clase o al\r\ncambio de éstas; y la no realización del aseo de los mismos', 1, '2017-04-06 16:17:29'),
(11, '22.11', 'La utilización del tiempo destinado a las actividades académicas en\r\njuegos o en acciones distractoras', 1, '2017-04-06 16:17:29'),
(12, '22.12', 'Las manifestaciones afectivas excesivas de cariño con sus compañeros\r\nque conlleve a mal entendidos', 1, '2017-04-06 16:17:29'),
(13, '22.13', 'El desorden en las formaciones', 1, '2017-04-06 16:17:29'),
(14, '22.14', 'Gritar a los compañeros', 1, '2017-04-06 16:17:29'),
(15, '22.15', 'Crear comentarios que deterioren la buena imagen de cualquiera de las\r\npersonas que conforman la familia Albertina (Profesores, Estudiantes,\r\nPadres de Familia, Servicios Generales, Administrativos, Directivos)', 1, '2017-04-06 16:17:29'),
(16, '22.16', 'Las murmuraciones, acusaciones o información falsa contra\r\nCompañeros, Docentes o cualquier miembro de la comunidad', 1, '2017-04-06 16:17:29'),
(17, '22.17', 'Retener las comunicaciones que envía el Colegio a los acudientes o\r\npadres de familia o viceversa', 1, '2017-04-06 16:17:29'),
(18, '22.18', 'El uso de objetos dentro del aula de clases tales como radios, revistas,\r\ngrabadoras, celulares, entre otras, que impidan el buen desarrollo de la\r\nclase', 1, '2017-04-06 16:17:29'),
(19, '22.19', 'Consumir alimentos en horas de clase y actividades académicas', 1, '2017-04-06 16:17:29'),
(20, '22.20', 'Hacer trazos, grafitis o escritos en los pupitres, ventanas, paredes,\r\nbaños, vehículos, dentro y/o fuera de la Institución', 1, '2017-04-06 16:17:29'),
(21, '22.21', 'Hacer caso omiso de los llamados de atención por parte de cualquier\r\nmiembro de la institución (Docentes , Directivos y de Servicio\r\nGenerales)', 1, '2017-04-06 16:17:29'),
(22, '22.22', 'Con la tercera llegada tarde, se llamará al acudiente para que informe\r\nla situación y tomar los correctivos disciplinarios pertinentes', 1, '2017-04-06 16:17:29'),
(23, '22.23', 'La no información a los padres de familia o acudientes de las citaciones\r\nverbales o escritas', 1, '2017-04-06 16:17:29'),
(24, '22.24', 'Registrar tres (3) anotaciones disciplinarias en el observador del\r\nalumno', 1, '2017-04-06 16:17:29'),
(25, '22.25', 'Realizar actos que atenten contra el orden y la seguridad de la\r\ncomunidad educativa dentro y fuera de la Institución', 1, '2017-04-06 16:17:29'),
(26, '22.26', 'El porte de celulares y cámaras fotográficas dentro de la Institución', 1, '2017-04-06 16:17:29'),
(27, '23.1', 'La reincidencia en faltas leves', 2, '2017-04-06 16:17:29'),
(28, '23.2', 'El hurto en todas sus manifestaciones', 2, '2017-04-06 16:17:29'),
(29, '23.3', 'El fraude, engaño o suplantación en evaluaciones, trabajos y actividades\r\nasignadas', 2, '2017-04-06 16:17:29'),
(30, '23.4', 'El irrespeto grave a Directivos, Profesores, Compañeros y demás\r\npersonas dentro o fuera del Colegio', 2, '2017-04-06 16:17:29'),
(31, '23.5', 'Los actos y expresiones de inmoralidad dentro y fuera del Colegio', 2, '2017-04-06 16:17:29'),
(32, '23.6', 'El porte, uso o suministro de armas de fuego, corto punzantes,\r\nexplosivos u otros elementos que ocasionen heridas y traumatismo y/o\r\ngeneren desorden.', 2, '2017-04-06 16:17:29'),
(33, '23.7', 'El consumo o distribución de bebidas embriagantes o drogas\r\nalucinógenas dentro o fuera del Colegio', 2, '2017-04-06 16:17:29'),
(34, '23.8', 'Falsificación de documentos de la Institución', 2, '2017-04-06 16:17:29'),
(35, '23.9', 'El porte o divulgación de pornografía, cualquiera que sea el medio\r\nempleado', 2, '2017-04-06 16:17:29'),
(36, '23.10', 'Las actuaciones fuera del Colegio que perjudiquen gravemente la\r\nbuena imagen de las personas y de la Institución', 2, '2017-04-06 16:17:29'),
(37, '23.11', 'El maltrato o abuso voluntario de los equipos, implementos didácticos\r\ny otros recursos del Colegio', 2, '2017-04-06 16:17:29'),
(38, '23.12', 'La incitación a la violencia y el desorden', 2, '2017-04-06 16:17:29'),
(39, '23.13', 'Intimidación, intento, acoso o soborno dentro y fuera del Colegio a\r\ncualquier miembro de la comunidad educativa', 2, '2017-04-06 16:17:29'),
(40, '23.14', 'La práctica de juegos de azar y de dinero', 2, '2017-04-06 16:17:29'),
(41, '23.15', 'La difamación del buen nombre e imagen de la Institución o de las\r\npersonas de la misma, a utilizar el nombre de estos en actividades sin\r\ndebido conocimiento y autorización', 2, '2017-04-06 16:17:29'),
(42, '23.16', 'Participar en grupos satánicos, metafísicos (mal intencionado),\r\nespiritismo o brujería', 2, '2017-04-06 16:17:29'),
(43, '23.17', 'El incumplimiento de cualquiera de sus deberes como estudiantes\r\nregistrados en este Manual de Convivencia', 2, '2017-04-06 16:17:29'),
(44, '23.18', 'Inducir o coaccionar a algún compañero (a) a consumir sustancias\r\nalucinógenas u otra que afecte su estado físico y mental', 2, '2017-04-06 16:17:29'),
(45, '23.19', 'Agresión física a cualquier miembro de la institución, especialmente a\r\nsus compañeros estudiantes', 2, '2017-04-06 16:17:29'),
(46, '23.20', 'Inducir o coaccionar a alguna estudiante que pueda estar en embarazo,\r\na un aborto', 2, '2017-04-06 16:17:29'),
(47, '23.21', 'Amenazar a cualquier miembro de la Institución, o poner en riesgo la\r\nvida de alguno de ellos', 2, '2017-04-06 16:17:29'),
(48, '23.22', 'Atentar contra los bienes muebles e inmuebles de toda la comunidad\r\neducativa', 2, '2017-04-06 16:17:29'),
(49, '23.23', 'Apoyar y ejecutar cualquier actividad que vaya en contra de la sana\r\nconvivencia y de la vida', 2, '2017-04-06 16:17:29'),
(50, '23.24', 'Sabotear las actividades culturales, académicas y sociales que programe\r\nel Colegio', 2, '2017-04-06 16:17:29'),
(51, '23.25', 'Los comentarios mal intencionados que permitan la discriminación\r\ncon cualquiera de los miembros de la institución, sean de tipo sexual,\r\ncultural, político, religioso, racial y otros que emanen los Derechos\r\nHumanos y la Constitución Política de Col', 2, '2017-04-06 16:17:29'),
(52, '23.26', 'Cualquier situación que afecte el buen nombre el Colegio San Alberto Magno, como consecuencia de catos que atentan contra las normas de convivencia social', 2, '2017-04-06 16:17:29'),
(53, '23.27', 'Realizar, promover o participar, sin la correspondiente autorización\r\nde Rectoría, cualquier género de ventas, rifas y negocios con los\r\nCompañeros, Profesores y otros funcionarios del plantel', 2, '2017-04-06 16:17:29'),
(54, '23.28', 'Utilizar la Internet o páginas sociales empleando lenguajes\r\ninapropiados en contra de la Institución o compañeros y/o entrar en\r\npáginas no autorizadas', 2, '2017-04-06 16:17:29'),
(55, '23.29', 'Faltas contra el proceso de aprendizaje: la constante indisciplina en\r\nel salón de clase (charlas, gritos, juegos, lanzar objetos, otros) y/o la\r\nindolencia académica', 2, '2017-04-06 16:17:29'),
(56, '23.30', 'La inasistencia injustificada a los eventos programados por la\r\nInstitución', 2, '2017-04-06 16:17:29'),
(57, '23.31', 'El vocabulario soez, los insultos, las afrentas, los sobrenombres, las\r\nburlas o ridiculizaciones con base a defectos, limitaciones, características\r\no actitudes de las personas', 2, '2017-04-06 16:17:29'),
(58, '23.32', 'Motivar o participar en peleas o enfrenamientos dentro o fuera del\r\nColegio', 2, '2017-04-06 16:17:29'),
(59, '23.33', 'La inasistencia injustificada a clases encontrándose dentro de la\r\nInstitución', 2, '2017-04-06 16:17:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fault_type`
--

CREATE TABLE `fault_type` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fault_type`
--

INSERT INTO `fault_type` (`id`, `des`, `ts`) VALUES
(1, 'Leves', '2017-03-30 15:48:00'),
(2, 'Graves', '2017-03-30 15:48:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grades`
--

CREATE TABLE `grades` (
  `id` int(2) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grades`
--

INSERT INTO `grades` (`id`, `des`, `ts`) VALUES
(1, 'Pre-Jardín', '2017-03-30 16:00:33'),
(2, 'Jardín', '2017-03-30 16:00:33'),
(3, 'Transición', '2017-03-30 16:00:33'),
(4, 'Primero', '2017-03-30 16:00:33'),
(5, 'Segundo', '2017-03-30 16:00:33'),
(6, 'Tercero', '2017-03-30 16:00:33'),
(7, 'Cuarto', '2017-03-30 16:00:33'),
(8, 'Quinto', '2017-03-30 16:00:33'),
(9, 'Sexto', '2017-03-30 16:00:33'),
(10, 'Séptimo', '2017-03-30 16:00:33'),
(11, 'Octavo', '2017-03-30 16:00:33'),
(12, 'Noveno', '2017-03-30 16:00:33'),
(13, 'Décimo', '2017-03-30 16:00:33'),
(14, 'Undécimo', '2017-05-25 15:10:15'),
(15, 'Ninguno', '2017-06-09 20:37:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `period`
--

CREATE TABLE `period` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `b_date` date DEFAULT NULL,
  `e_date` date DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `period`
--

INSERT INTO `period` (`id`, `des`, `b_date`, `e_date`, `ts`) VALUES
(1, 'IPeriodo', '2017-02-01', '2017-04-07', '2017-05-25 15:11:10'),
(2, 'IIPeriodo', '2017-04-17', '2017-06-16', '2017-03-30 15:17:09'),
(3, 'IIIPeriodo', '2017-07-13', '2017-09-15', '2017-03-30 15:17:09'),
(4, 'IVPeriodo', '2017-09-18', '2017-11-17', '2017-05-25 15:15:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `tdni` varchar(250) DEFAULT NULL,
  `dni` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `pass_word` char(32) DEFAULT NULL,
  `id_user_type` int(1) DEFAULT NULL,
  `id_grades` int(2) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `tdni`, `dni`, `name`, `last_name`, `email`, `pass_word`, `id_user_type`, `id_grades`, `ts`) VALUES
(1, 'NUIP  ', '1048075080', 'VALERY DANIELA', 'GARCÍA SEPULVEDA', 'cindysepulveda0508@hotmail.com', '1048075080', 1, 4, '2017-06-16 15:09:32'),
(2, 'NUIP  ', '1046705478', 'YERLIS LUCIA', 'GONZALEZ SEHUANES', 'x@hotmail.com', '1046705478', 1, 4, '2017-06-16 15:09:32'),
(3, 'NUIP  ', '1044637611', 'BREINER ANDRES', 'GUTIERREZ VILLAREAL', '', '1044637611', 1, 4, '2017-06-16 15:09:32'),
(4, 'NUIP  ', '1130272783', 'SHEILY CAROLINA', 'HERRERA BELTRAN', 'de-iver@hotmail.com', '1130272783', 1, 4, '2017-06-16 15:09:33'),
(5, 'NUIP  ', '1043162053', 'SHAROLCK ', 'LLAMAS LARIOS', 'deysonllamas@hotmail.com', '1043162053', 1, 4, '2017-06-16 15:09:33'),
(6, 'NUIP  ', '1043455102', 'ALEJANDRA SOFIA', 'MARIMON GRANADOS', 'alexandrag3009@hotmail.com', '1043455102', 1, 4, '2017-06-16 15:09:33'),
(7, 'NUIP  ', '1043457257', 'ALEJANDRA ISABEL', 'MENDOZA SOTO', 'yysotop@hotmail.com', '1043457257', 1, 4, '2017-06-16 15:09:33'),
(8, 'NUIP  ', '1044641887', 'JOEL ', 'MERCADO GALOFRE', 'rubygalofre@hotmail.com', '1044641887', 1, 4, '2017-06-16 15:09:33'),
(9, 'NUIP  ', '1042859538', 'ISABELLA ANDREA', 'CASTRO PINO', 'ameliapino@hotmail.com', '1042859538', 1, 4, '2017-06-16 15:09:33'),
(10, 'NUIP  ', '1044220692', 'JONATHAN ALEMAO', 'COLMENARES PARRA', 'x@hotmail.com', '1044220692', 1, 4, '2017-06-16 15:09:33'),
(11, 'NUIP  ', '1043683387', 'SHARON ORIANA', 'CORTES FONTALVO', 'shirlyfontalvo-03@hotmail.com', '1043683387', 1, 4, '2017-06-16 15:09:33'),
(12, 'NUIP  ', '1139432283', 'SCARLET JULISA', 'PEREZ BOADA', 'xxxx@HOTMAIL.COM', '1139432283', 1, 4, '2017-06-16 15:09:33'),
(13, 'NUIP  ', '1139432730', 'GABRIELA VALENTINA', 'PEREZ GUTIERREZ', '', '1139432730', 1, 4, '2017-06-16 15:09:33'),
(14, 'NUIP  ', '1131284053', 'GABRIEL ANDRES', 'PICO RODRIGUEZ', 'xxxx@hotmail.com', '1131284053', 1, 4, '2017-06-16 15:09:33'),
(15, 'NUIP  ', '1045729968', 'KIMBERLY ESTHER', 'QUIROGA BASTIDAS', 'x@hotmail.com', '1045729968', 1, 4, '2017-06-16 15:09:33'),
(16, 'R.C.  ', '1043682898', 'KRISTIAN ', 'SALAS POLO', 'LAURAVPOLO07@HOTMAIL.COM', '1043682898', 1, 4, '2017-06-16 15:09:33'),
(17, 'R.C.  ', '1044642380', 'JOYCE FERNANDA', 'SALAZAR PUA', 'JOLIE2011@HOTMAIL.COM', '1044642380', 1, 4, '2017-06-16 15:09:33'),
(18, 'R.C.  ', '1047230665', 'BREINER DANIEL', 'TORRES ALVAREZ', 'sandramilena@hotmail.es', '1047230665', 1, 4, '2017-06-16 15:09:33'),
(19, 'NUIP  ', '1044640006', 'LUIS EDUARDO', 'CORZO BUELVAS', 'XXX@HOTMAIL.COM', '1044640006', 1, 4, '2017-06-16 15:09:33'),
(20, 'NUIP  ', '1050549967', 'YISED ', 'FELIZZOLA PEREZ', 'beatrizperez1903@gmail.com', '1050549967', 1, 4, '2017-06-16 15:09:33'),
(21, 'R.C.  ', '1043454713', 'ANNYES VALENTINA', 'MARQUEZ RODRIGUEZ', 'nkadri1310@gmail.com', '1043454713', 1, 4, '2017-06-16 15:09:33'),
(22, 'R.C.  ', '1047049533', 'LUCAS PAUL', 'MIRANDA AGUILAR', '', '1047049533', 1, 4, '2017-06-16 15:09:33'),
(23, 'NUIP  ', '1127596604', 'THAIS CAMILA', 'REDONDO PERTUZ', 'xxxxxxx@hotmail.com', '1127596604', 1, 4, '2017-06-16 15:09:33'),
(24, 'NUIP  ', '1028871163', 'KANNER IVAN', 'VINASCO GARCIA', 'kayeesgahu@gmail.com', '1028871163', 1, 4, '2017-06-16 15:09:33'),
(25, 'NUIP  ', '1043683357', 'LAYERSON DE JESUS', 'GALVIS MEDINA', 'ESTHERGUZMAN05854@HOTMAIL.COM', '1043683357', 1, 4, '2017-06-16 15:09:33'),
(26, 'NUIP  ', '1048075051', 'CARLOS ANDRES', 'MARTINEZ MURILLO', 'chekande25@gmail.com', '1048075051', 1, 4, '2017-06-16 15:09:33'),
(27, 'NUIP  ', '1066520740', 'MIGUEL ÁNGEL', 'VERGARA BETTIN', 'mayebettin@hotmail.com', '1066520740', 1, 4, '2017-06-16 15:09:33'),
(28, 'NUIP  ', '1043453795', 'BRIAN SMITH', 'CAMARGO FONTALVO', '', '1043453795', 1, 4, '2017-06-16 15:09:33'),
(29, 'NUIP  ', '1043682381', 'JUAN JOSE', 'CANOLES GONZALEZ', 'CARMEN_ANGELMIO@HOTMAIL.COM', '1043682381', 1, 4, '2017-06-16 15:09:33'),
(30, 'NUIP  ', '1043452392', 'JHOSUA ', 'ECHEVERRIA RIVERA', 'socadellevalentinacorcho@gmail.com', '1043452392', 1, 4, '2017-06-16 15:09:33'),
(31, 'NUIP  ', '1046709137', 'LINA GABRIELA', 'PARDO PEÑA', 'jaque-laulina@hotmail.com', '1046709137', 1, 4, '2017-06-16 15:09:33'),
(32, 'R.C.  ', '1037126766', 'TORIBIO ALBERTO', 'PAUTH MOSQUERA', 'ANAMOSRU@HOTMAIL.COM', '1037126766', 1, 4, '2017-06-16 15:09:33'),
(33, 'NUIP  ', '1043456800', 'SEBASTIAN ANDRES', 'SABALZA HERRERA', 'johor23@hotmail.com', '1043456800', 1, 4, '2017-06-16 15:09:33'),
(34, 'T.I.  ', '1002159322', 'ANGEL DAVID', 'BRAVO AGUILAR', 'LUZMARYAGUILARSARABIA@HOTMAIL.COM', '1002159322', 1, 13, '2017-06-16 15:09:33'),
(35, 'T.I.  ', '1001918561', 'HAMILTON YESID', 'BANQUEZ PASTRANA', 'XXX@HOTMAIL.COM', '1001918561', 1, 13, '2017-06-16 15:09:33'),
(36, 'T.I.  ', '1002212685', 'JESUS ', 'CIFUENTE VIZCAINO', '', '1002212685', 1, 13, '2017-06-16 15:09:33'),
(37, 'T.I.  ', '1001947315', 'MARIA DEL CARMEN', 'ESCOBAR MARRUGO', 'naesca90@hotmail.com', '1001947315', 1, 13, '2017-06-16 15:09:33'),
(38, 'T.I.  ', '1001946085', 'MICHELLE ARANTXA', 'FLORIAN NAVARRO', 'SAMUFLOU@HOTMAIL.COM', '1001946085', 1, 13, '2017-06-16 15:09:33'),
(39, 'T.I.  ', '1001944118', 'CAMILO ANDRES', 'GALINDO MENCO', 'x@hotmail.com', '1001944118', 1, 13, '2017-06-16 15:09:34'),
(40, 'T.I.  ', '1001887467', 'JESUS MIGUEL', 'HERRERA SALAZAR', '', '1001887467', 1, 13, '2017-06-16 15:09:34'),
(41, 'T.I.  ', '1001853794', 'MANUEL SEBASTIAN', 'LEAL CALDERON', 'calderonrnm@hotmail.com', '1001853794', 1, 13, '2017-06-16 15:09:34'),
(42, 'T.I.  ', '1002092830', 'BRANDON SEBASTIAN', 'LLAMAS LARIOS', 'deysonllamas@hotmail.com', '1002092830', 1, 13, '2017-06-16 15:09:34'),
(43, 'T.I.  ', '1001624432', 'ANNY YISETH', 'MARTINEZ ANDRADE', 'xxx@hotmail.com', '1001624432', 1, 13, '2017-06-16 15:09:34'),
(44, 'T.I.  ', '1002159554', 'LAURENIS ', 'MONTENEGRO GONZALEZ', '', '1002159554', 1, 13, '2017-06-16 15:09:34'),
(45, 'T.I.  ', '1001856797', 'SANTIAGO ', 'PERALTA ARDILA', 'x@hotmail.com', '1001856797', 1, 13, '2017-06-16 15:09:34'),
(46, 'C.C.  ', '1143165505', 'LINA ROSA', 'PEREZ BOLAÑOS', 'XXXXXXXX@HOTMAIL.COM', '1143165505', 1, 13, '2017-06-16 15:09:34'),
(47, 'T.I.  ', '1001943375', 'DIEGO ANDRES', 'ROLONG LOZANO', 'diemon_79@hotmail.com', '1001943375', 1, 13, '2017-06-16 15:09:34'),
(48, 'T.I.  ', '1001916524', 'KEYNS DAYANA', 'ROMERO MALDONADO', '', '1001916524', 1, 13, '2017-06-16 15:09:34'),
(49, 'T.I.  ', '1043114082', 'LORAINE ANDREA', 'SANTIZ CORONELL', 'xxxx@hotmail.com', '1043114082', 1, 13, '2017-06-16 15:09:34'),
(50, 'T.I.  ', '1001999279', 'LAURA VANESSA', 'SERRANO LOPEZ', '', '1001999279', 1, 13, '2017-06-16 15:09:34'),
(51, 'T.I.  ', '1043434528', 'JONATHAN JOSÉ', 'VARGAS DELGADO', 'SANDRADELGADO2326@HOTMAIL.COM', '1043434528', 1, 13, '2017-06-16 15:09:34'),
(52, 'T.I.  ', '1001779171', 'MAURO GABRIEL', 'VILORIA ITURRIAGO', '', '1001779171', 1, 13, '2017-06-16 15:09:34'),
(53, 'T.I.  ', '1001912232', 'LUIS ANDRES', 'YEPES CARDENAS', 'noralbacardenas@hotmail.com', '1001912232', 1, 13, '2017-06-16 15:09:34'),
(54, 'T.I.  ', '1001999131', 'CAMILO ANDRES', 'BARRIOS QUIROZ', 'albertoalba2003@yahoo.com', '1001999131', 1, 13, '2017-06-16 15:09:34'),
(55, 'T.I.  ', '1001940516', 'KARINA SOFIA', 'GONZALEZ OJEDA', 'XXX@HOTMAIL.COM', '1001940516', 1, 13, '2017-06-16 15:09:34'),
(56, 'T.I.  ', '1007120802', 'HENDRITH LEONARDO', 'VELASQUEZ MARTELO', 'xxxxxx@hotmail.com', '1007120802', 1, 13, '2017-06-16 15:09:34'),
(57, 'T.I.  ', '1001958942', 'ELIAN ALBERTO', 'CAMPO ARELLANA', 'x@hotmail.com', '1001958942', 1, 13, '2017-06-16 15:09:34'),
(58, 'T.I.  ', '1001824220', 'KEIDDY MARIA', 'GARCIA LOPEZ', 'X@HOTMAIL.COM', '1001824220', 1, 13, '2017-06-16 15:09:34'),
(59, 'T.I.  ', '1007986973', 'ELIAN DAVID', 'MONSALVE PEREZ', '', '1007986973', 1, 13, '2017-06-16 15:09:34'),
(60, 'T.I.  ', '1001915784', 'EDGAR DUVAN', 'AHUMADA URIBE', '', '1001915784', 1, 14, '2017-06-16 15:09:34'),
(61, 'T.I.  ', '1192925727', 'LEONARDO ANDRES', 'ARCOS GARCIA', 'MONISABELJJLLSS@HOTMAIL.COM', '1192925727', 1, 14, '2017-06-16 15:09:34'),
(62, 'T.I.  ', '1001856994', 'JORGE ANDRES', 'BORJA SERRANO', 'kattysofia2011@hotmail.com', '1001856994', 1, 14, '2017-06-16 15:09:34'),
(63, 'T.I.  ', '1002154456', 'LUIS ANGEL', 'FERNANDEZ ZAMORA', '', '1002154456', 1, 14, '2017-06-16 15:09:34'),
(64, 'T.I.  ', '1193444911', 'JOSEPH JAIR', 'FLOREZ LOPEZ', 'x@hotmail.com', '1193444911', 1, 14, '2017-06-16 15:09:34'),
(65, 'T.I.  ', '98072069188', 'JUAN DAVID', 'GARCIA ', 'cpgarcia@hotmail.com', '98072069188', 1, 14, '2017-06-16 15:09:34'),
(66, 'T.I.  ', '1002157547', 'YERSON CAMILO', 'GUZMAN RODRIGUEZ', '', '1002157547', 1, 14, '2017-06-16 15:09:35'),
(67, 'T.I.  ', '1001820868', 'MARIA ALEJANDRA', 'MEZA MOLINA', '', '1001820868', 1, 14, '2017-06-16 15:09:35'),
(68, 'T.I.  ', '1001914895', 'CARLOS ALBERTO', 'NORIEGA ECHEVERRIA', '', '1001914895', 1, 14, '2017-06-16 15:09:35'),
(69, 'T.I.  ', '1002162661', 'NATALIA ANDREA', 'PERALTA PEREZ', 'x@hotmail.com', '1002162661', 1, 14, '2017-06-16 15:09:35'),
(70, 'T.I.  ', '1001779059', 'DAVID ALEJANDRO', 'PEREIRA ATENCIA', 'maryjulys.roldan@gmail.com', '1001779059', 1, 14, '2017-06-16 15:09:35'),
(71, 'T.I.  ', '1192767873', 'BREINER ', 'RODRIGUEZ GIL', 'x@hotmail.com', '1192767873', 1, 14, '2017-06-16 15:09:35'),
(72, 'T.I.  ', '1001820930', 'LUIS DANIEL', 'TERAN MORENO', 'XXXX@HOTMAIL.COM', '1001820930', 1, 14, '2017-06-16 15:09:35'),
(73, 'T.I.  ', '1002152745', 'HUGO ANDRES', 'TINOCO NOGUERA', 'deisy2180@hotmail.com', '1002152745', 1, 14, '2017-06-16 15:09:35'),
(74, 'T.I.  ', '1005480532', 'DARWIN EFRAIN', 'WANDURRAGA VELASQUEZ', '', '1005480532', 1, 14, '2017-06-16 15:09:35'),
(75, 'T.I.  ', '1192781055', 'JUAN DAVID', 'PASTRANA BARRIOS', 'xxx@hotmail.com', '1192781055', 1, 14, '2017-06-16 15:09:35'),
(76, 'T.I.  ', '1001821114', 'ANTONIO JOSÉ', 'PERALTA BONADIEZ', 'JACEBOCA@GMAIL.COM', '1001821114', 1, 14, '2017-06-16 15:09:35'),
(77, 'T.I.  ', '1001879191', 'YULEIMA JAZMIN', 'TAFUR GOMEZ', '', '1001879191', 1, 14, '2017-06-16 15:09:35'),
(78, 'T.I.  ', '1010122634', 'VALENTINA ', 'VILLARREAL DE LA HOZ', 'mariadelahoz7733@hotmail.com', '1010122634', 1, 14, '2017-06-16 15:09:35'),
(79, 'NUIP  ', '1045702697', 'SANTIAGO ', 'BETANCOURT TAPIA', 'andre0214@hotmail.com', '1045702697', 1, 5, '2017-06-16 15:09:35'),
(80, 'NUIP  ', '1096805611', 'ANDRES FELIPE', 'BUSTAMANTE CASTILLO', 'vane110380@hotmail.com', '1096805611', 1, 5, '2017-06-16 15:09:35'),
(81, 'NUIP  ', '1048078165', 'LIKAR CAMILO', 'CAMPO RODRIGUEZ', 'xxx@hotmail.com', '1048078165', 1, 5, '2017-06-16 15:09:35'),
(82, 'R.C.  ', '1043449705', 'EMMANUEL JESUS', 'CANO MARIOTA', 'ladysdecano@gmail.com', '1043449705', 1, 5, '2017-06-16 15:09:35'),
(83, 'NUIP  ', '1041695722', 'LUZ DIVINA', 'CARBAL SIERRA', 'luzyouangel-83@hotmail.com', '1041695722', 1, 5, '2017-06-16 15:09:35'),
(84, 'R.C.  ', '1042262124', 'MATIAS ', 'CERVANTES PEREZ', 'KELLYS_83@YAHOO.ES', '1042262124', 1, 5, '2017-06-16 15:09:35'),
(85, 'R.C.  ', '1139431300', 'GABRIELA ', 'CHINCHIA PERALTA', 'XIOMYPH19@HOTMAIL.COM', '1139431300', 1, 5, '2017-06-16 15:09:35'),
(86, 'R.C.  ', '1046706249', 'ALAN DANIEL', 'ESTEVEZ RUBIO', 'lyer.27@outlook.es', '1046706249', 1, 5, '2017-06-16 15:09:35'),
(87, 'NUIP  ', '1143129274', 'HEIDY CAROLINA', 'GONZALEZ CEBALLOS', 'heidyceballos@outlook.es', '1143129274', 1, 5, '2017-06-16 15:09:35'),
(88, 'NUIP  ', '1046706479', 'LUCAS TONY', 'GONZALEZ SEHUANES', 'x@hotmail.com', '1046706479', 1, 5, '2017-06-16 15:09:35'),
(89, 'R.C.  ', '1143241184', 'MATHIAS JOSE', 'HERNANDEZ BERMUDEZ', 'cido-06@hotmail.com', '1143241184', 1, 5, '2017-06-16 15:09:35'),
(90, 'NUIP  ', '1051826208', 'HERNANDO JAVIER', 'HERNANDEZ SIERRA', 'ana.sierra0102@gmail.com', '1051826208', 1, 5, '2017-06-16 15:09:35'),
(91, 'R.C.  ', '1158463481', 'DANIELA ', 'HOYOS BARRIOS', 'lfalquez2008@hotmail.com', '1158463481', 1, 5, '2017-06-16 15:09:35'),
(92, 'NUIP  ', '1043452117', 'ISABELLA ANDREA', 'JIMENEZ PATRON', '', '1043452117', 1, 5, '2017-06-16 15:09:36'),
(93, 'R.C.  ', '1140844103', 'IBRAYN ALIN', 'MARTINEZ JANNE', 'x@hotmail.com', '1140844103', 1, 5, '2017-06-16 15:09:36'),
(94, 'NUIP  ', '1046705247', 'GABRIELA DE LOS ANGELES', 'MARTINEZ NAVARRO', 'XX@HOTMAIL.COM', '1046705247', 1, 5, '2017-06-16 15:09:36'),
(95, 'NUIP  ', '1048074286', 'MATIAS DAVID', 'MARTINEZ ROYERO', 'yuri3royero@hotmail.com', '1048074286', 1, 5, '2017-06-16 15:09:36'),
(96, 'R.C.  ', '1043683090', 'MARK DIONY', 'MERCADO CHARRIS', 'lichar71@hotmail.com', '1043683090', 1, 5, '2017-06-16 15:09:36'),
(97, 'R.C.  ', '1046706227', 'LAURA SOFIA', 'MIRANDA TORRENEGRA', 'SHICA05@HOTMAIL.COM', '1046706227', 1, 5, '2017-06-16 15:09:36'),
(98, 'NUIP  ', '1042858262', 'MATIAS DAVID', 'OLMOS GOMEZ', 'x@hotmail.com', '1042858262', 1, 5, '2017-06-16 15:09:36'),
(99, 'NUIP  ', '1143353677', 'YULIETH SOFIA', 'QUINTANA OLIVARES', '', '1143353677', 1, 5, '2017-06-16 15:09:36'),
(100, 'NUIP  ', '1042857400', 'ALEJANDRO DAVID', 'RICAURTE COLMENARES', 'lindacolmenares0609@hotmail.com', '1042857400', 1, 5, '2017-06-16 15:09:36'),
(101, 'R.C.  ', '1044219981', 'ALAN DAVID', 'ROJAS VILLALOBO', 'villalobosbeatriz@hotmail.com', '1044219981', 1, 5, '2017-06-16 15:09:36'),
(102, 'NUIP  ', '1047045938', 'LUIS ESTEBAN', 'ROYERO ROMERO', 'x@hotmail.com', '1047045938', 1, 5, '2017-06-16 15:09:36'),
(103, 'R.C.  ', '1139430992', 'LUIS ANGEL', 'URIBE ESTOR', 'YULEST22@GMAIL.COM', '1139430992', 1, 5, '2017-06-16 15:09:36'),
(104, 'NUIP  ', '1048212522', 'ROYMAR ARMANDO', 'VARELA COMAS', 'MIFONO0123@HOTMAIL.COM', '1048212522', 1, 5, '2017-06-16 15:09:36'),
(105, 'NUIP  ', '1042260526', 'YHARA MARGARITA', 'VASQUEZ FERRER', '', '1042260526', 1, 5, '2017-06-16 15:09:36'),
(106, 'R.C.  ', '1139431493', 'SAJOHA PAOLA', 'VASQUEZ RODRIGUEZ', 'Johana.rodriguez.meza@hotmail.com', '1139431493', 1, 5, '2017-06-16 15:09:36'),
(107, 'NUIP  ', '1016718067', 'MIGUEL ANTONIO', 'VILLEGAS DOMINGUEZ', 'emmadmv@hotmail.com', '1016718067', 1, 5, '2017-06-16 15:09:36'),
(108, 'R.C.  ', '1082949191', 'SARA ALEJANDRA', 'ORTIZ OSORIO', 'YISY-JUSTIN@HOTMAIL.COM', '1082949191', 1, 5, '2017-06-16 15:09:36'),
(109, 'NUIP  ', '1046706929', 'MARIA JOSE', 'LONDOÑO RAMOS', 'cenithramos@hotmail.com', '1046706929', 1, 5, '2017-06-16 15:09:36'),
(110, 'NUIP  ', '1048074558', 'MARIA VICTORIA', 'MEDINA VILLARREAL', 'villarrealdelahoz22@hotmail.com', '1048074558', 1, 5, '2017-06-16 15:09:36'),
(111, 'NUIP  ', '1201216420', 'SAMUEL DAVID', 'PARDO SALTARIN', 'tatianasm19@gmail.com', '1201216420', 1, 5, '2017-06-16 15:09:36'),
(112, 'R.C.  ', '1043678254', 'MELANIE SOFIA', 'BARBOSA LEAL', 'x@hotmail.com', '1043678254', 1, 6, '2017-06-16 15:09:36'),
(113, 'T.I.  ', '1042257322', 'JOSE LUIS', 'BERTEL POLO', 'JOSELUISBERTELPOLO@HOTMAIL.COM', '1042257322', 1, 6, '2017-06-16 15:09:36'),
(114, 'NUIP  ', '1044631740', 'VALERIA ESTER', 'CANTILLO HURTADO', '', '1044631740', 1, 6, '2017-06-16 15:09:36'),
(115, 'NUIP  ', '1044633667', 'CARLOS EDUARDO', 'CORZO BUELVAS', 'XXX@HOTMAIL.COM', '1044633667', 1, 6, '2017-06-16 15:09:36'),
(116, 'NUIP  ', '1043451531', 'CAMILO ANDRES', 'CRUZ GONZALEZ', 'lusgoqui@hotmail.com', '1043451531', 1, 6, '2017-06-16 15:09:36'),
(117, 'R.C.  ', '1194963505', 'MELANIE AIDES', 'GARCIA MENDOZA', '', '1194963505', 1, 6, '2017-06-16 15:09:36'),
(118, 'NUIP  ', '1048073305', 'DAILYN MISHELL', 'GUTIERREZ BONETT', '', '1048073305', 1, 6, '2017-06-16 15:09:36'),
(119, 'NUIP  ', '1046704769', 'TANYA ALEJANDRA', 'CANOLES OLASCOAGA', 'x@hotmail.com', '1046704769', 1, 6, '2017-06-16 15:09:37'),
(120, 'R.C.  ', '1096804498', 'MARIA VALENTINA', 'HERNANDEZ RIPOLL', 'LIC.KARENRIPOLL@HOTMAIL.COM', '1096804498', 1, 6, '2017-06-16 15:09:37'),
(121, 'R.C.  ', '1044630764', 'DINA LUZ', 'JIMENEZ MAESTRE', 'JOMADI1@HOTMAIL.COM', '1044630764', 1, 6, '2017-06-16 15:09:37'),
(122, 'R.C.  ', '1042257946', 'LUCIANA MARGARITA', 'MEJIA MEJIA', 'x@hotmail.com', '1042257946', 1, 6, '2017-06-16 15:09:37'),
(123, 'T.I.  ', '1043475291', 'SILFREDO ANTONIO', 'MONTENEGRO OJEDA', 'ivanmadridojeda@outlook.es', '1043475291', 1, 6, '2017-06-16 15:09:37'),
(124, 'R.C.  ', '1044218711', 'BETSY PAOLA', 'MUNERA PEREZ', 'LUISMUNERA0718@HOTMAIL.COM', '1044218711', 1, 6, '2017-06-16 15:09:37'),
(125, 'NUIP  ', '1043676058', 'MARIANA SOFIA', 'POLO MIRANDA', 'melinamiranda27@hotmail.es', '1043676058', 1, 6, '2017-06-16 15:09:37'),
(126, 'NUIP  ', '1047046313', 'YHERSON ', 'RICARDO GUTIERREZ', 'yunismargarita14@hotmail.com', '1047046313', 1, 6, '2017-06-16 15:09:37'),
(127, 'R.C.  ', '1045704001', 'MELANIE ISABEL', 'SAUCEDO ROMERO', 'yelisau3101@hotmail.com', '1045704001', 1, 6, '2017-06-16 15:09:37'),
(128, 'R.C.  ', '1043451239', 'VALENTINA SOFIA', 'TIRIA JIMENEZ', 'x@hotmail.com', '1043451239', 1, 6, '2017-06-16 15:09:37'),
(129, 'R.C.  ', '1043451414', 'HELLEN JOHANA', 'TORRES SANTIS', 'SATISJOHANA@HOTMAIL.COM', '1043451414', 1, 6, '2017-06-16 15:09:37'),
(130, 'R.C.  ', '1048071411', 'VICTOR MODESTO', 'VIAÑA AYOS', '', '1048071411', 1, 6, '2017-06-16 15:09:37'),
(131, 'R.C.  ', '1043676827', 'LIA VALENTINA', 'VILLALOBOS VERGEL', '', '1043676827', 1, 6, '2017-06-16 15:09:37'),
(132, 'T.I.  ', '1044219306', 'GIBSY SAYLI', 'ALTAMAR IGLESIAS', 'pocahontasvip@hotmail.com', '1044219306', 1, 6, '2017-06-16 15:09:37'),
(133, 'NUIP  ', '1043451045', 'KESMAN JORETH', 'FLOREZ DIAZ', '', '1043451045', 1, 6, '2017-06-16 15:09:37'),
(134, 'NUIP  ', '1041695441', 'SEBASTIAN ', 'HERNANDEZ GALVIS', '', '1041695441', 1, 6, '2017-06-16 15:09:37'),
(135, 'R.C.  ', '1140835780', 'JUAN DIEGO', 'LOPEZ BARRIOS', 'YURIBARRIO38@GMAIL.COM', '1140835780', 1, 6, '2017-06-16 15:09:37'),
(136, 'NUIP  ', '1043451156', 'ALEJANDRO JOSE', 'MEJÍA ROLONG', 'danitza_rolong_2209@hotmail.com', '1043451156', 1, 6, '2017-06-16 15:09:37'),
(137, 'R.C.  ', '1127596603', 'EMELYN PAOLA', 'REDONDO PERTUZ', 'xxxxxxx@hotmail.com', '1127596603', 1, 6, '2017-06-16 15:09:37'),
(138, 'NUIP  ', '1042856041', 'DIEGO ENRIQUE', 'RODRIGUEZ GIL', 'x@hotmail.com', '1042856041', 1, 6, '2017-06-16 15:09:37'),
(139, 'T.I.  ', '1140834068', 'CAMILO ANDRES', 'ROJANO BARRIOS', 'JNARVAE3493@GMAIL.COM', '1140834068', 1, 6, '2017-06-16 15:09:37'),
(140, 'R.C.  ', '1044630646', 'TALIANA MICHEL', 'SANTIAGO PAJARO', 'ohmadtar@hotmail.com', '1044630646', 1, 6, '2017-06-16 15:09:37'),
(141, 'NUIP  ', '1044631140', 'LAURA MELISA', 'CARVAJAL COLMENARES', 'LUCIA05071@HOTMAIL.COM', '1044631140', 1, 6, '2017-06-16 15:09:37'),
(142, 'NUIP  ', '1044633607', 'MATIAS DANIEL', 'GUTIERREZ CABALLERO', 'mayiasesorcomercial@hotmail.com', '1044633607', 1, 6, '2017-06-16 15:09:37'),
(143, 'T.I.  ', '1041692877', 'AFFETH ', 'LANS HERRERA', 'ameslans@hotmail.com', '1041692877', 1, 6, '2017-06-16 15:09:37'),
(144, 'T.I.  ', '1143238356', 'ELIANA MARCELA', 'MEJIA BOLIVAR', 'CINDYBOLIVARC@HOTMAIL.COM', '1143238356', 1, 6, '2017-06-16 15:09:37'),
(145, 'T.I.  ', '1048074147', 'SHARON NICOLL', 'MONSALVE PEREZ', '', '1048074147', 1, 6, '2017-06-16 15:09:37'),
(146, 'R.C.  ', '1048070411', 'JUAN CAMILO', 'ALMANZA GAMARRA', '', '1048070411', 1, 7, '2017-06-16 15:09:37'),
(147, 'NUIP  ', '1042854024', 'GENESIS PATRICIA', 'CAMPO ARELLANA', 'x@hotmail.com', '1042854024', 1, 7, '2017-06-16 15:09:37'),
(148, 'R.C.  ', '1043447609', 'HILARY SOFIA', 'ACUÑA VERGARA', 'x@hotmail.com', '1043447609', 1, 7, '2017-06-16 15:09:37'),
(149, 'T.I.  ', '1043445855', 'HANSEL DAVID', 'AGRESOT MEDINA', 'martaines.2009@hotmail.com', '1043445855', 1, 7, '2017-06-16 15:09:37'),
(150, 'NUIP  ', '1046702269', 'YUSBLEIDIS SAYOHAR', 'ARIAS ALVAREZ', '', '1046702269', 1, 7, '2017-06-16 15:09:37'),
(151, 'R.C.  ', '1042253374', 'BREINER DANILSON', 'ARISTIZABAL ROMO', 'XILENA2509@HOTMAIL.COM', '1042253374', 1, 7, '2017-06-16 15:09:37'),
(152, 'NUIP  ', '1044624454', 'HANNAH ', 'ARIZA ESTEVEZ', 'dj27h-h@hotmail.com', '1044624454', 1, 7, '2017-06-16 15:09:37'),
(153, 'T.I.  ', '1143116628', 'JOYBER DANIEL', 'BELTRAN FONTALVO', 'elpesao_02@hotmail.com', '1143116628', 1, 7, '2017-06-16 15:09:37'),
(154, 'NUIP  ', '1042253066', 'GUILLERMO ANDRES', 'BERMUDEZ MARTINEZ', 'mr33418@gmail.com', '1042253066', 1, 7, '2017-06-16 15:09:38'),
(155, 'R.C.  ', '1044627339', 'NICOLL ESTHER', 'BONOLI ALCENDRA', 'x@hotmail.com', '1044627339', 1, 7, '2017-06-16 15:09:38'),
(156, 'R.C.  ', '1082921753', 'LUNA VALENTINA', 'BOTTED ARIZA', 'nlbotted@gmail.com', '1082921753', 1, 7, '2017-06-16 15:09:38'),
(157, 'R.C.  ', '1082902565', 'NOHELIA JOHANNA', 'BOTTED ARIZA', 'nlbotted@gmail.com', '1082902565', 1, 7, '2017-06-16 15:09:38'),
(158, 'NUIP  ', '1043676289', 'BRIGEETH CAMILA', 'CAMPO RODRIGUEZ', 'xxx@hotmail.com', '1043676289', 1, 7, '2017-06-16 15:09:38'),
(159, 'NUIP  ', '1139427700', 'ADRIAN RAMIRO', 'CONEO MARIN', 'adrianconeo28@hotmail.com', '1139427700', 1, 7, '2017-06-16 15:09:38'),
(160, 'NUIP  ', '1043676251', 'VALERY DANIELA', 'DIAZ IZQUIERDO', 'graneropaola@hotmail.com', '1043676251', 1, 7, '2017-06-16 15:09:38'),
(161, 'R.C.  ', '1043673842', 'JONATHAN DAVID', 'FLOREZ RAMBAO', 'LACHIKYARE78@HOTMAIL.COM', '1043673842', 1, 7, '2017-06-16 15:09:38'),
(162, 'T.I.  ', '1123207879', 'JUAN CAMILO', 'GONZALEZ CEBALLOS', 'heidyceballos@outlook.es', '1123207879', 1, 7, '2017-06-16 15:09:38'),
(163, 'T.I.  ', '1044624130', 'HAROLD LEONARD', 'GUTIERREZ QUESADA', 'adri0722@hotmail.com', '1044624130', 1, 7, '2017-06-16 15:09:38'),
(164, 'T.I.  ', '1047225095', 'JOSÉ ANGEL', 'MAESTRE POLANCO', 'VICTORIAMAESTREPOLANCO@HOTMAIL.COM', '1047225095', 1, 7, '2017-06-16 15:09:38'),
(165, 'R.C.  ', '1047226053', 'MANUEL ALEJANDRO', 'MEJIA DIAZ', 'alejando-2010@hotmail.com', '1047226053', 1, 7, '2017-06-16 15:09:38'),
(166, 'NUIP  ', '1044623621', 'CRISTIAN MIGUEL', 'MORALES HORTA', 'MONICAHORTAN@HOTMAIL.COM', '1044623621', 1, 7, '2017-06-16 15:09:38'),
(167, 'R.C.  ', '1041694116', 'CAROLAY ADRIANA', 'PAREDES PEREZ', 'x@hotmail.com', '1041694116', 1, 7, '2017-06-16 15:09:38'),
(168, 'R.C.  ', '1042257148', 'MARY ANGEL', 'FERNANDEZ SUAREZ', 'AASUAREZ7@MISENA.EDU.CO', '1042257148', 1, 7, '2017-06-16 15:09:38'),
(169, 'R.C.  ', '1030559968', 'EVER SANTIAGO', 'PULGARIN MEDINA', 'GINACAROMEDINA@YAHOO.COM', '1030559968', 1, 7, '2017-06-16 15:09:38'),
(170, 'NUIP  ', '1048070490', 'RAFAEL ISAAC', 'RAMIREZ SEPULVEDA', 'cindysepulveda0508@hotmail.com', '1048070490', 1, 7, '2017-06-16 15:09:38'),
(171, 'T.I.  ', '1139428575', 'BREYNER ANDRES', 'ROJANO RUEDA', 'breiner311@hotmail.com', '1139428575', 1, 7, '2017-06-16 15:09:38'),
(172, 'T.I.  ', '1044625528', 'DANNY DE JESUS', 'VARGAS POLANCO', 'kendo-2@hotmail.com', '1044625528', 1, 7, '2017-06-16 15:09:38'),
(173, 'T.I.  ', '1042853883', 'MARIA ANGEL', 'VILLARREAL DE LA HOZ', 'mariadelahoz7733@hotmail.com', '1042853883', 1, 7, '2017-06-16 15:09:38'),
(174, 'T.I.  ', '1044626668', 'MATEO ANDRES', 'PERTUZ ROBLES', 'nicolas.pertuz@hotmail.com', '1044626668', 1, 7, '2017-06-16 15:09:38'),
(175, 'T.I.  ', '1042257118', 'SEBASTIAN ', 'VALDERRAMA HOYOS', '', '1042257118', 1, 7, '2017-06-16 15:09:38'),
(176, 'T.I.  ', '1043446495', 'SEBASTIAN ANDRES', 'MIRANDA NIÑO', 'xxxx@hotmail.com', '1043446495', 1, 7, '2017-06-16 15:09:38'),
(177, 'NUIP  ', '1042255466', 'JESHUA YADIEL', 'MONTESINO TAPIA', 'dayje12@hotmail.com', '1042255466', 1, 7, '2017-06-16 15:09:38'),
(178, 'T.I.  ', '1044621072', 'OMAR SEBASTIAN', 'OSPINO BLANCO', 'DIANUSE@HOTMAIL.COM', '1044621072', 1, 7, '2017-06-16 15:09:38'),
(179, 'NUIP  ', '1044628517', 'KAYWIL HENRY', 'PARRA HOYOS', '', '1044628517', 1, 7, '2017-06-16 15:09:38'),
(180, 'NUIP  ', '1043677221', 'SANTIAGO DANIEL', 'PEDROZO TAPIA', 'iristapiasn@gmail.com', '1043677221', 1, 7, '2017-06-16 15:09:38'),
(181, 'NUIP  ', '1043445799', 'EDIER DE JESUS', 'PEÑA JIMENO', 'maritzajimeno@hotmail.com', '1043445799', 1, 7, '2017-06-16 15:09:38'),
(182, 'R.C.  ', '1043445144', 'SEBASTIAN ', 'SALCEDO CERVANTES', '', '1043445144', 1, 7, '2017-06-16 15:09:38'),
(183, 'NUIP  ', '1043675116', 'ALBERTO ESTEBAN', 'VASQUEZ POLO', 'quevas2275@gmail.com', '1043675116', 1, 7, '2017-06-16 15:09:38'),
(184, 'T.I.  ', '1047041061', 'CAMILA ALEXANDRA', 'BANQUEZ PASTRANA', 'XXX@HOTMAIL.COM', '1047041061', 1, 8, '2017-06-16 15:09:38'),
(185, 'T.I.  ', '1042248284', 'ASLY CAROLINA', 'BERMUDEZ MARTINEZ', 'mr33418@gmail.com', '1042248284', 1, 8, '2017-06-16 15:09:39'),
(186, 'T.I.  ', '1042248283', 'CECILIA ANDREA', 'BERMUDEZ MARTINEZ', 'mr33418@gmail.com', '1042248283', 1, 8, '2017-06-16 15:09:39'),
(187, 'T.I.  ', '1139426986', 'ISABELLA ', 'BERRIO AMADOR', 'ASTRIDAMADOR@COLSAM.EDU.CO', '1139426986', 1, 8, '2017-06-16 15:09:39'),
(188, 'T.I.  ', '1139426600', 'CAMILO JOSE', 'BUSTAMANTE GUZMAN', 'DAMA_GUZ28@HOTMAIL.COM', '1139426600', 1, 8, '2017-06-16 15:09:39'),
(189, 'NUIP  ', '1047043235', 'VALERIE DE JESUS', 'CASTRO MOLINA', 'carmen_2010@hotmail.es', '1047043235', 1, 8, '2017-06-16 15:09:39'),
(190, 'T.I.  ', '1044214576', 'KEYLEEN ARIANA', 'COBA LOPEZ', 'janethlopez352@gmail.com', '1044214576', 1, 8, '2017-06-16 15:09:39'),
(191, 'T.I.  ', '1047043564', 'SAID HABIB', 'COLEY ROMERO', 'licero16@hotmail.com', '1047043564', 1, 8, '2017-06-16 15:09:39'),
(192, 'T.I.  ', '1042250056', 'FREDY ESTEBAN', 'CORPAS LOPEZ', 'eneidalopezorozco@gmail.com', '1042250056', 1, 8, '2017-06-16 15:09:39'),
(193, 'NUIP  ', '1044623397', 'ADRIANA LUCIA', 'DE LEON ARROYO', 'lucia05071@hotmail.com', '1044623397', 1, 8, '2017-06-16 15:09:39'),
(194, 'NUIP  ', '1047042181', 'KAYDI LORENA', 'DUNCAN MOLINA', 'kary-2009@live.com', '1047042181', 1, 8, '2017-06-16 15:09:39'),
(195, 'NUIP  ', '1042250398', 'MARKO ALEJANDRO', 'ENCISO VANEGAS', 'XXXX@HOTMAIL.COM', '1042250398', 1, 8, '2017-06-16 15:09:39'),
(196, 'T.I.  ', '1042252247', 'MARIANA ', 'FERNANDEZ SUAREZ', 'AASUAREZ7@MISENA.EDU.CO', '1042252247', 1, 8, '2017-06-16 15:09:39'),
(197, 'T.I.  ', '1047040302', 'JESUS MANUEL', 'GARCIA DE LA HOZ', 'lacande1414@outlook.com', '1047040302', 1, 8, '2017-06-16 15:09:39'),
(198, 'T.I.  ', '1042250139', 'ARMANDO ANDRÉS', 'GARCÍA ARRIETA', 'sandra.arrieta14@hotmail.com', '1042250139', 1, 8, '2017-06-16 15:09:39'),
(199, 'T.I.  ', '1048069898', 'DANIEL ESTEBAN', 'GOMEZ CHAMORRO', 'catherine.chamorro@hotmail.com', '1048069898', 1, 8, '2017-06-16 15:09:39'),
(200, 'NUIP  ', '1042851306', 'CAMILO ANDRES', 'GUERRA HERRERA', '', '1042851306', 1, 8, '2017-06-16 15:09:39'),
(201, 'T.I.  ', '1044624240', 'RONALD DANIEL', 'MARTINEZ CANTILLO', 'NADIACAN1980@HOTMAIL.COM', '1044624240', 1, 8, '2017-06-16 15:09:39'),
(202, 'T.I.  ', '1139426510', 'LAURA VANESSA', 'MEDRANO LOZANO', 'CHELAVANESA@HOTMAIL.COM', '1139426510', 1, 8, '2017-06-16 15:09:39'),
(203, 'T.I.  ', '1043671269', 'LIZDANIS PAOLA', 'MOLINA IZQUIERDO', 'graneropaola@hotmail.com', '1043671269', 1, 8, '2017-06-16 15:09:39'),
(204, 'R.C.  ', '1044215962', 'YANDIEL ', 'MUNERA PEREZ', 'LUISMUNERA0718@HOTMAIL.COM', '1044215962', 1, 8, '2017-06-16 15:09:39'),
(205, 'T.I.  ', '1043668683', 'CLAUDIA PATRICIA', 'OROZCO BELTRAN', 'mariaorozco66@hotmail.com', '1043668683', 1, 8, '2017-06-16 15:09:39'),
(206, 'T.I.  ', '1048069587', 'ALEJANDRA ', 'PEÑA ROA', 'NAREDROA@GMAIL.COM', '1048069587', 1, 8, '2017-06-16 15:09:39'),
(207, 'T.I.  ', '1042852118', 'MARIA CLAUDIA', 'PERALTA PEREZ', 'x@hotmail.com', '1042852118', 1, 8, '2017-06-16 15:09:39'),
(208, 'R.C.  ', '1139426361', 'MARINA ANDREA', 'PEREZ GUTIERREZ', '', '1139426361', 1, 8, '2017-06-16 15:09:39'),
(209, 'T.I.  ', '1047043597', 'JESÚS MANUEL', 'PINTO ARTETA', 'yurysarteta@gmail.com', '1047043597', 1, 8, '2017-06-16 15:09:39'),
(210, 'T.I.  ', '1143331331', 'FABIAN JOSÉ', 'QUINTANA OLIVARES', '', '1143331331', 1, 8, '2017-06-16 15:09:39'),
(211, 'T.I.  ', '1044621300', 'DAINER ANTONIO', 'QUINTANA PALMA', '', '1044621300', 1, 8, '2017-06-16 15:09:39'),
(212, 'T.I.  ', '1043666109', 'JUAN CAMILO', 'RAMIREZ QUINTERO', 'xxxx@hotmail.com', '1043666109', 1, 8, '2017-06-16 15:09:39'),
(213, 'T.I.  ', '1043443205', 'VALERY LILIANA', 'SANABRIA GALVIS', '', '1043443205', 1, 8, '2017-06-16 15:09:39'),
(214, 'T.I.  ', '1044215202', 'ALEJANDRA MARIA', 'SERRANO MESA', '', '1044215202', 1, 8, '2017-06-16 15:09:39'),
(215, 'T.I.  ', '1043445405', 'MATEO ANDRES', 'TORRES SANTIS', 'SATISJOHANA@HOTMAIL.COM', '1043445405', 1, 8, '2017-06-16 15:09:40'),
(216, 'NUIP  ', '1042251861', 'LAURA CRISTINA', 'URREA PARRA', 'x@hotmail.com', '1042251861', 1, 8, '2017-06-16 15:09:40'),
(217, 'T.I.  ', '1042250157', 'MARIANA SOFIA', 'ARISTIZABAL CUETO', 'claudiac1980@hotmail.es', '1042250157', 1, 9, '2017-06-16 15:09:40'),
(218, 'T.I.  ', '1043439807', 'ANNDY ', 'GARCIA LONDOÑO', 'joice-0520@hotmail.com', '1043439807', 1, 9, '2017-06-16 15:09:40'),
(219, 'T.I.  ', '1044618176', 'CESAR ISAAC', 'ACOSTA GARCIA', 'carmen.garcia4571@gmail.com', '1044618176', 1, 9, '2017-06-16 15:09:40'),
(220, 'T.I.  ', '1043439221', 'JUAN ESTEBAN', 'ACOSTA VENEGAS', 'MARICARMEN.VENEGAS@HOTMAIL.COM', '1043439221', 1, 9, '2017-06-16 15:09:40'),
(221, 'T.I.  ', '1098456019', 'MARIA FERNANDA', 'ANGARITA PICO', 'XXX@HOTMAIL.COM', '1098456019', 1, 9, '2017-06-16 15:09:40'),
(222, 'T.I.  ', '1073976135', 'DARCY LILIANA', 'DAVILA MARTINEZ', 'XXXX@HOTMAIL.COM', '1073976135', 1, 9, '2017-06-16 15:09:40'),
(223, 'T.I.  ', '1042851141', 'JUAN CAMILO', 'FERNANDEZ BARRIOS', 'CARMEN-BM-15@HOTMAIL.COM', '1042851141', 1, 9, '2017-06-16 15:09:40'),
(224, 'T.I.  ', '1043441393', 'WEINCES NAYID', 'HERNANDEZ PEREZ', 'KELLYS_83@YAHOO.ES', '1043441393', 1, 9, '2017-06-16 15:09:40'),
(225, 'T.I.  ', '1043442056', 'MARIA FERNANDA', 'HERRERA MANZANO', 'sumamunicipios@gmail.com', '1043442056', 1, 9, '2017-06-16 15:09:40'),
(226, 'T.I.  ', '1042250318', 'ZHARICK JOHANA', 'INSIGNARES DELGADO', 'SANDRADELGADO2326@HOTMAIL.COM', '1042250318', 1, 9, '2017-06-16 15:09:40'),
(227, 'T.I.  ', '1043435984', 'HERNAN ALBERTO', 'JIMENEZ AVILA', '', '1043435984', 1, 9, '2017-06-16 15:09:40'),
(228, 'T.I.  ', '1043437292', 'JESUS DAVID', 'JIMENEZ AVILA', '', '1043437292', 1, 9, '2017-06-16 15:09:40'),
(229, 'T.I.  ', '1044613075', 'KAREN ANDREA', 'MORALES HORTA', 'MONICAHORTAN@HOTMAIL.COM', '1044613075', 1, 9, '2017-06-16 15:09:40'),
(230, 'NUIP  ', '1042853169', 'MARGARITA SAUDITH', 'OSPINO DE LOS REYES', 'carlosalberto1972@hotmail.es', '1042853169', 1, 9, '2017-06-16 15:09:40'),
(231, 'NUIP  ', '40361756', 'JAIRON XAVIER', 'PARDO ARAUJO', 'x@hotmail.com', '40361756', 1, 9, '2017-06-16 15:09:40'),
(232, 'T.I.  ', '1044614415', 'LAURA VANESSA', 'SANTIZ CORONELL', 'xxxx@hotmail.com', '1044614415', 1, 9, '2017-06-16 15:09:40'),
(233, 'T.I.  ', '1044615857', 'JOHAN ESTIBEN', 'WANDURRAGA VELASQUEZ', '', '1044615857', 1, 9, '2017-06-16 15:09:40'),
(234, 'NUIP  ', '1043663777', 'RAFAEL HERNANDO', 'LLANOS MUGNO', 'x@hotmail.com', '1043663777', 1, 9, '2017-06-16 15:09:40'),
(235, 'T.I.  ', '1042247352', 'JOSE SEBASTIAN', 'LONDOÑO RAMOS', 'cenithramos@hotmail.com', '1042247352', 1, 9, '2017-06-16 15:09:40'),
(236, 'T.I.  ', '1042851151', 'SAMUEL CAMILO', 'SAMPAYO GONZALEZ', 'mayg1203@gmail.com', '1042851151', 1, 9, '2017-06-16 15:09:40'),
(237, 'T.I.  ', '1042248542', 'SOFIA ', 'TERAN MORENO', 'XXXX@HOTMAIL.COM', '1042248542', 1, 9, '2017-06-16 15:09:40'),
(238, 'T.I.  ', '1044212709', 'ALEJANDRO ISAAC', 'GOMEZ VARGAS', 'XXXXXX@HOTMAIL.COM', '1044212709', 1, 9, '2017-06-16 15:09:40'),
(239, 'T.I.  ', '1044604111', 'SHARICK ', 'PAJARO MORENO', '', '1044604111', 1, 9, '2017-06-16 15:09:40'),
(240, 'T.I.  ', '1042248841', 'ANA SOFIA', 'PARRA LONDOÑO', 'greyslg0105@hotmail.com', '1042248841', 1, 9, '2017-06-16 15:09:40'),
(241, 'T.I.  ', '1044606765', 'RUTH MARIA', 'RIVERA RIQUEME', 'geovanyrivera12@hotmail.com', '1044606765', 1, 9, '2017-06-16 15:09:40'),
(242, 'T.I.  ', '1042852421', 'ANGEL DAVID', 'RUIZ ORTEGA', 'MARTHA-ORTEGA24@HOTMAIL.COM', '1042852421', 1, 9, '2017-06-16 15:09:40'),
(243, 'T.I.  ', '1044608244', 'JAVIER REYNALDO', 'VERA RUSSO', 'IMPORTACIONESMOLINOS@YAHOO.ES', '1044608244', 1, 9, '2017-06-16 15:09:40'),
(244, 'T.I.  ', '239655', 'KEREN SARAY', 'ALMANZA GAMARRA', '', '239655', 1, 10, '2017-06-16 15:09:41'),
(245, 'T.I.  ', '1001999272', 'ALDAIR ', 'ATENCIA CAMACHO', 'cvbred@hotmail.com', '1001999272', 1, 10, '2017-06-16 15:09:41'),
(246, 'NUIP  ', '1082852421', 'SHAIEL ALEXANDRA', 'BUENO CATALAN', 'luzmarycatalancarvajal@gmail.com', '1082852421', 1, 10, '2017-06-16 15:09:41'),
(247, 'T.I.  ', '104605685', 'SANTIAGO ', 'CANOLES OLASCOAGA', 'x@hotmail.com', '104605685', 1, 10, '2017-06-16 15:09:41'),
(248, 'T.I.  ', '1042244844', 'JUAN DAVID', 'GAMARRA CASTILLO', 'marjoriecastillo344@hotmail.com', '1042244844', 1, 10, '2017-06-16 15:09:41'),
(249, 'T.I.  ', '1044604638', 'JOSE YAMID', 'ARMESTO ROMERO', '', '1044604638', 1, 10, '2017-06-16 15:09:41'),
(250, 'T.I.  ', '1043146150', 'MICHELL GABRIELA', 'BOLAÑO VELASQUEZ', 'xxx@hotmail.com', '1043146150', 1, 10, '2017-06-16 15:09:41'),
(251, 'T.I.  ', '1043437303', 'REYNALDO ', 'CALA DE LA ROSA', 'XXXX@HOTMAIL.COM', '1043437303', 1, 10, '2017-06-16 15:09:41'),
(252, 'T.I.  ', '1042246974', 'LANDY ANDREA', 'CARO JIMENEZ', 'yuranisjimenezbolivar@hotmail.com', '1042246974', 1, 10, '2017-06-16 15:09:41'),
(253, 'T.I.  ', '1048065942', 'KEISSY LILIANA', 'CERMEÑO BARRIOS', 'kellybarrios1112@hotmail.com', '1048065942', 1, 10, '2017-06-16 15:09:41'),
(254, 'T.I.  ', '1001997387', 'ISAAC DANIEL', 'CHAVEZ ORTIZ', 'WILVILMA123@HOTMAIL.COM', '1001997387', 1, 10, '2017-06-16 15:09:41'),
(255, 'T.I.  ', '1123801562', 'DAYAN MISHEL', 'ECHEVERRY CAMACHO', '', '1123801562', 1, 10, '2017-06-16 15:09:41'),
(256, 'R.C.  ', '1042242945', 'JOELDY DAVID', 'FLOREZ LOPEZ', 'x@hotmail.com', '1042242945', 1, 10, '2017-06-16 15:09:41'),
(257, 'T.I.  ', '1044608934', 'MAYSSA ', 'GALEANO ZAPATA', 'MAYSSA_84@HOTMAIL.COM', '1044608934', 1, 10, '2017-06-16 15:09:41'),
(258, 'T.I.  ', '1044606141', 'JORDY JOSÈ', 'GALVIS MEDINA', 'ESTHERGUZMAN05854@HOTMAIL.COM', '1044606141', 1, 10, '2017-06-16 15:09:41'),
(259, 'T.I.  ', '1041770681', 'JHONATHAN STEVEN', 'GARCIA TORRES', 'jgjg29@hotmial.com', '1041770681', 1, 10, '2017-06-16 15:09:41'),
(260, 'T.I.  ', '1043121445', 'SHARICK ', 'LLAMAS LARIOS', 'deysonllamas@hotmail.com', '1043121445', 1, 10, '2017-06-16 15:09:41'),
(261, 'T.I.  ', '1044421082', 'OSCAR GUILLERMO', 'MANCERA TEHERAN', 'samy-temu@hotmail.com', '1044421082', 1, 10, '2017-06-16 15:09:41'),
(262, 'T.I.  ', '1193093133', 'VERONICA MARIA', 'MORALES HORTA', 'MONICAHORTAN@HOTMAIL.COM', '1193093133', 1, 10, '2017-06-16 15:09:41'),
(263, 'T.I.  ', '1139424312', 'RICARDO AIMAR', 'NAVARRO CABALLERO', 'marielacaba01@hotmail.com', '1139424312', 1, 10, '2017-06-16 15:09:41'),
(264, 'T.I.  ', '1042248663', 'MARIANGEL ', 'OLEA OSPINO', '', '1042248663', 1, 10, '2017-06-16 15:09:41'),
(265, 'T.I.  ', '1046703986', 'CHELSEA FRANCINE', 'PADILLA VEGA', '', '1046703986', 1, 10, '2017-06-16 15:09:41'),
(266, 'T.I.  ', '1043693797', 'SARA MICHELL', 'PALMA BARRIOS', 'georys_palma@hotmail.com', '1043693797', 1, 10, '2017-06-16 15:09:41'),
(267, 'T.I.  ', '1046694543', 'SEBASTIAN ', 'RICARDO GUTIERREZ', 'yunismargarita14@hotmail.com', '1046694543', 1, 10, '2017-06-16 15:09:41'),
(268, 'T.I.  ', '1048065980', 'ROBERTO JOSE', 'RODRIGUEZ BARROS', 'tefiro914@hotmail.com', '1048065980', 1, 10, '2017-06-16 15:09:41'),
(269, 'T.I.  ', '1041531220', 'ESTEFANIA ', 'SERNA CARTAGENA', 'BEATRIZCARTAGENACARTAGENA@GMAIL.COM', '1041531220', 1, 10, '2017-06-16 15:09:41'),
(270, 'T.I.  ', '1129506037', 'MARITZABEL ', 'SERNA ROSADO', '', '1129506037', 1, 10, '2017-06-16 15:09:41'),
(271, 'T.I.  ', '1041691865', 'PAULA MARGARITA', 'VALDES GUTIERREZ', 'linagutierrez2007@hotmail.com', '1041691865', 1, 10, '2017-06-16 15:09:42'),
(272, 'T.I.  ', '1041974947', 'NANCY MARIA', 'VELASQUEZ MARTELO', 'xxxxxx@hotmail.com', '1041974947', 1, 10, '2017-06-16 15:09:42'),
(273, 'T.I.  ', '1044606315', 'CALEB GERARDO', 'VERA AMAYA', 'AMAYAYOLANDA@HOTMAIL.COM', '1044606315', 1, 10, '2017-06-16 15:09:42'),
(274, 'NUIP  ', '1043661699', 'KEVIN ', 'RODRIGUEZ GONZALEZ', 'x@hotmail.com', '1043661699', 1, 10, '2017-06-16 15:09:42'),
(275, 'T.I.  ', '1001856009', 'BENJAMIN JUNIOR', 'CANOLES OLASCOAGA', 'x@hotmail.com', '1001856009', 1, 11, '2017-06-16 15:09:42'),
(276, 'T.I.  ', '1193593206', 'MARIA CLAUDIA', 'CARRASQUILLA ARIZA', 'CLARIZAARIZA@HOTMAIL.COM', '1193593206', 1, 11, '2017-06-16 15:09:42'),
(277, 'T.I.  ', '1001824513', 'LUIS ANGEL', 'CUELLAR BALLESTEROS', 'ARCANGEL030@HOTMAIL.COM', '1001824513', 1, 11, '2017-06-16 15:09:42'),
(278, 'T.I.  ', '1044607154', 'LAUREN ISABEL ', 'DE LA CRUZ VALDES', '', '1044607154', 1, 11, '2017-06-16 15:09:42'),
(279, 'T.I.  ', '1001947574', 'JEAN BREYNER', 'FRANCO HERNANDEZ', '', '1001947574', 1, 11, '2017-06-16 15:09:42'),
(280, 'T.I.  ', '1001782087', 'CLAUDIA MARCELA', 'GUTIERREZ BLANCO', 'HERGUTI66@HOTMAIL.COM', '1001782087', 1, 11, '2017-06-16 15:09:42'),
(281, 'T.I.  ', '1043663513', 'YORLEIDYS SARAY', 'HERRERA MORALES', 'yorley1023@hotmail.com', '1043663513', 1, 11, '2017-06-16 15:09:42'),
(282, 'T.I.  ', '1001825136', 'MARIA ALEJANDRA', 'HOLGUIN BELTRAN', 'malejaholguin@hotmail.com', '1001825136', 1, 11, '2017-06-16 15:09:42'),
(283, 'T.I.  ', '1042240135', 'MAYRA ALEJANDRA', 'GUTIERREZ ALTAMIRANDA', 'yal6790@gmail.com', '1042240135', 1, 11, '2017-06-16 15:09:42'),
(284, 'T.I.  ', '1002134734', 'YUSBELYS DAYANA', 'ARIAS ALVAREZ', '', '1002134734', 1, 11, '2017-06-16 15:09:42'),
(285, 'T.I.  ', '1007175483', 'CARLOS ALBERTO', 'JIMENEZ AVILA', '', '1007175483', 1, 11, '2017-06-16 15:09:42'),
(286, 'T.I.  ', '1042848198', 'DYLAN MAURICIO', 'MONTES PEREA', 'liliana.perea@hotmail.com', '1042848198', 1, 11, '2017-06-16 15:09:42'),
(287, 'T.I.  ', '1193587502', 'INGRID JIMENA', 'TRUJILLO BENAVIDEZ', 'SHIRLY1992@HOTMAIL.ES', '1193587502', 1, 11, '2017-06-16 15:09:42'),
(288, 'T.I.  ', '1193102348', 'YUNAISY DE JESUS', 'HERNANDEZ CARDENAS', '', '1193102348', 1, 11, '2017-06-16 15:09:42'),
(289, 'T.I.  ', '1048064060', 'JOSE ALEJANDRO', 'MARQUEZ CARDENAS', '', '1048064060', 1, 11, '2017-06-16 15:09:42'),
(290, 'T.I.  ', '1042578201', 'AARON ', 'CABRALES CONTRERAS', 'x@hotmail.com', '1042578201', 1, 11, '2017-06-16 15:09:42'),
(291, 'T.I.  ', '1001933333', 'MARIA FERNANDA', 'VELASQUEZ GARCIA', 'cpgarcia@hotmail.com', '1001933333', 1, 11, '2017-06-16 15:09:42'),
(292, 'T.I.  ', '1102634216', 'YERIKA VIVIANA', 'DE LA HOZ GONZALEZ', 'mytanis@hotmail.es', '1102634216', 1, 11, '2017-06-16 15:09:42'),
(293, 'T.I.  ', '1043662491', 'JUAN CAMILO', 'CARBONELL HEILBRON', 'elyke25@hotmail.com', '1043662491', 1, 11, '2017-06-16 15:09:42'),
(294, 'T.I.  ', '1007125263', 'FRANCISCO ANTONIO', 'JIMENEZ JIMENEZ', 'cieneguita76@gmail.com', '1007125263', 1, 11, '2017-06-16 15:09:42'),
(295, 'T.I.  ', '1042849500', 'LEIDY ISABEL', 'BERDUGO MARTINEZ', 'yese828@HOTMAIL.COM', '1042849500', 1, 11, '2017-06-16 15:09:42'),
(296, 'T.I.  ', '1001946710', 'SEBASTIAN ', 'CAMARGO MARTINEZ', 'monik240909@hotmail.com', '1001946710', 1, 11, '2017-06-16 15:09:42'),
(297, 'T.I.  ', '1001854604', 'EDWIN LEONARDO', 'VILLARREAL MIRANDA', 'evil31@hotmail.com', '1001854604', 1, 11, '2017-06-16 15:09:42'),
(298, 'T.I.  ', '1001947975', 'JOSE ANGEL', 'JIMENEZ PADILLA', 'XXX@HOTMAIL.COM', '1001947975', 1, 11, '2017-06-16 15:09:42'),
(299, 'T.I.  ', '1042241356', 'JESUS MANUEL', 'ROBAYO PEREZ', 'TATO0324@HOTMAIL.ES', '1042241356', 1, 11, '2017-06-16 15:09:42'),
(300, 'T.I.  ', '1047035864', 'STEFANY ', 'CANO VASCO', 'L_27LUCIA2012@HOTMAIL.COM', '1047035864', 1, 11, '2017-06-16 15:09:43'),
(301, 'T.I.  ', '1001820198', 'PABLO ANGEL', 'LARIOS VERDOOREN', '', '1001820198', 1, 11, '2017-06-16 15:09:43'),
(302, 'T.I.  ', '1042242796', 'JUAN DAVID', 'PEREZ VILLALOBOS', 'XXX@HOTMAIL.COM', '1042242796', 1, 11, '2017-06-16 15:09:43'),
(303, 'T.I.  ', '1043672548', 'KELSY MARIA', 'FONTALVO VILLANUEVA', 'XXXX@GMAIL.COM', '1043672548', 1, 11, '2017-06-16 15:09:43'),
(304, 'T.I.  ', '1002132333', 'JULIO CESAR', 'RIBALDO GAMARRA', 'NARLENSTILO@HOTMAIL.COM', '1002132333', 1, 11, '2017-06-16 15:09:43'),
(305, 'T.I.  ', '1001917481', 'VALERIA SOFIA', 'ARBOLEDA HERRERA', '', '1001917481', 1, 12, '2017-06-16 15:09:43'),
(306, 'T.I.  ', '1001893210', 'ANDREA CAROLINA', 'ARRIETA BROCHERO', '', '1001893210', 1, 12, '2017-06-16 15:09:43'),
(307, 'T.I.  ', '1001915654', 'JOSE ALEJANDRO', 'BAYONA OJITO', 'jbayona401@hotmail.com', '1001915654', 1, 12, '2017-06-16 15:09:43'),
(308, 'T.I.  ', '1001918552', 'JANINNA ISABEL', 'BAZA ARRIETA', 'NAIRARRIETA72@HOTMAIL.COM', '1001918552', 1, 12, '2017-06-16 15:09:43'),
(309, 'T.I.  ', '1007894047', 'CRISTIAN DE JESUS', 'CAMARGO DE CARO', 'x@hotmail.com', '1007894047', 1, 12, '2017-06-16 15:09:43'),
(310, 'T.I.  ', '1001997922', 'AILEEN KARINA', 'CASTRO LOPEZ', '', '1001997922', 1, 12, '2017-06-16 15:09:43'),
(311, 'T.I.  ', '1001824401', 'KEIMMY MARIA', 'GARCIA LOPEZ', 'X@HOTMAIL.COM', '1001824401', 1, 12, '2017-06-16 15:09:43'),
(312, 'T.I.  ', '1002136788', 'DANIEL ENRIQUE', 'GLEN CARDALES', 'XXX@HOTMAIL.COM', '1002136788', 1, 12, '2017-06-16 15:09:43'),
(313, 'T.I.  ', '1001946234', 'LAURA VANESSA', 'LLANOS OROZCO', 'RUBIES10@HOTMAIL.COM', '1001946234', 1, 12, '2017-06-16 15:09:43'),
(314, 'T.I.  ', '1002159837', 'LINA MARCELA', 'MORON MUÑOZ', 'maria_mb_19@hotmail.com', '1002159837', 1, 12, '2017-06-16 15:09:43'),
(315, 'T.I.  ', '1002031194', 'KEVIN SEBASTIAN', 'PADILLA SARMIENTO', 'x@hotmail.com', '1002031194', 1, 12, '2017-06-16 15:09:43'),
(316, 'T.I.  ', '1001918400', 'JESUS ANTONIO', 'CANO MARIOTA', 'ladysdecano@gmail.com', '1001918400', 1, 12, '2017-06-16 15:09:43'),
(317, 'T.I.  ', '1001824516', 'GABRIELA ANDREA', 'PARRA LONDOÑO', 'greyslg0105@hotmail.com', '1001824516', 1, 12, '2017-06-16 15:09:43'),
(318, 'T.I.  ', '1002227047', 'VALENTINA MARIA', 'GARCIA TORRES', 'jgjg29@hotmial.com', '1002227047', 1, 12, '2017-06-16 15:09:43'),
(319, 'T.I.  ', '1002030424', 'VALENTINA ', 'IBAÑEZ MIRANDA', 'melinamiranda27@hotmail.es', '1002030424', 1, 12, '2017-06-16 15:09:43'),
(320, 'T.I.  ', '1007892978', 'LAURA VANESSA', 'LOPEZ HERNANDEZ', 'norishernandez71@gmail.com', '1007892978', 1, 12, '2017-06-16 15:09:43'),
(321, 'T.I.  ', '1001999196', 'LUIS DAVID', 'MUNERA PEREZ', 'LUISMUNERA0718@HOTMAIL.COM', '1001999196', 1, 12, '2017-06-16 15:09:43'),
(322, 'T.I.  ', '1001854844', 'CAROLINA ', 'NAVARRO LOPEZ', 'javier.navarrocaceres@hotmail.com', '1001854844', 1, 12, '2017-06-16 15:09:43'),
(323, 'T.I.  ', '1042240609', 'VALERIA MARIA', 'PINZON BARRIOS', '', '1042240609', 1, 12, '2017-06-16 15:09:43'),
(324, 'T.I.  ', '1002027132', 'DAIRO JOSE', 'QUINTANA OLIVARES', '', '1002027132', 1, 12, '2017-06-16 15:09:43'),
(325, 'T.I.  ', '1002160329', 'MARIA ALEJANDRA', 'TEJEDOR CACERES', 'nubiacm4924@yahoo.com', '1002160329', 1, 12, '2017-06-16 15:09:44'),
(326, 'T.I.  ', '1001942937', 'YURY MARCELA', 'TORRES HENRIQUEZ', 'patriciahenriquezgarcia_123@hotmail.com', '1001942937', 1, 12, '2017-06-16 15:09:44'),
(327, 'T.I.  ', '1001854725', 'BRITHNI JHULIANA', 'VANEGAS OSORIO', 'x@hotmail.com', '1001854725', 1, 12, '2017-06-16 15:09:44'),
(328, 'T.I.  ', '1001945646', 'VALERIA ', 'MIRANDA TORRENEGRA', 'SHICA05@HOTMAIL.COM', '1001945646', 1, 12, '2017-06-16 15:09:44'),
(329, 'T.I.  ', '1043434117', 'YERIBETH MARIA', 'PALENCIA BOLIVAR', 'x@hotmail.com', '1043434117', 1, 12, '2017-06-16 15:09:44'),
(330, 'T.I.  ', '1001998330', 'LUIS DAVID', 'VERA MEDINA', 'x@hotmail.com', '1001998330', 1, 12, '2017-06-16 15:09:44'),
(331, 'T.I.  ', '1001822847', 'JOYMAR MANUEL', 'VILLA BONILLA', 'M.AIRABONILLA@HOTMAIL.COM', '1001822847', 1, 12, '2017-06-16 15:09:44'),
(332, 'R.C.  ', '1047050866', 'JUAN DIEGO', 'CAMARGO DE CARO', 'x@hotmail.com', '1047050866', 1, 2, '2017-06-16 15:09:44'),
(333, 'NUIP  ', '1044648716', 'ANA SHARAY', 'ACOSTA GARCIA', 'carmen.garcia4571@gmail.com', '1044648716', 1, 2, '2017-06-16 15:09:44'),
(334, 'R.C.  ', '1042264814', 'ALEJANDRO ', 'AVILA DE LA ROSA', 'NADEA0704@HOTMAIL.COM', '1042264814', 1, 2, '2017-06-16 15:09:44'),
(335, 'NUIP  ', '1046716372', 'SOFIA ANDREA', 'BETANCOURT TAPIA', 'andre0214@hotmail.com', '1046716372', 1, 2, '2017-06-16 15:09:44'),
(336, 'R.C.  ', '1048077982', 'MARIA JOSE', 'BUSTAMANTE GUZMAN', 'DAMA_GUZ28@HOTMAIL.COM', '1048077982', 1, 2, '2017-06-16 15:09:44'),
(337, 'NUIP  ', '1044222930', 'KERHEEN SOFIA', 'COBA LOPEZ', 'janethlopez352@gmail.com', '1044222930', 1, 2, '2017-06-16 15:09:44'),
(338, 'NUIP  ', '1046714712', 'JAIRO DE JESÚS', 'DE ORO PERNET', 'SULGEYPERNETT@HOTMAIL.COM', '1046714712', 1, 2, '2017-06-16 15:09:44'),
(339, 'NUIP  ', '1043688928', 'SEBASTIAN ANDRES', 'FELIPE MONTENEGRO', '', '1043688928', 1, 2, '2017-06-16 15:09:44'),
(340, 'NUIP  ', '1046716198', 'MARIA DANIELA', 'GARCIA LOZANO', '', '1046716198', 1, 2, '2017-06-16 15:09:44'),
(341, 'NUIP  ', '1046715858', 'KEREN DANIELA', 'GONZALEZ HERRERA', 'KERENDANIELA24@GMAIL.COM', '1046715858', 1, 2, '2017-06-16 15:09:44'),
(342, 'NUIP  ', '1046715549', 'SOFIA MARIANA', 'GUERRERO HERNANDEZ', 'ANGEL2909@HOTMAIL.ES', '1046715549', 1, 2, '2017-06-16 15:09:44'),
(343, 'R.C.  ', '1043689519', 'VALENTINA ', 'JIMENEZ PADILLA', 'XXX@HOTMAIL.COM', '1043689519', 1, 2, '2017-06-16 15:09:44'),
(344, 'NUIP  ', '1043691341', 'ALFONSO JOSÉ', 'PACHECO PEREZ', '', '1043691341', 1, 2, '2017-06-16 15:09:44'),
(345, 'NUIP  ', '1043462157', 'FABIAN ZAID', 'PAREDES PEREZ', 'x@hotmail.com', '1043462157', 1, 2, '2017-06-16 15:09:44'),
(346, 'NUIP  ', '1046714278', 'JUAN DAVID', 'RUA MARTINEZ', 'monik240909@hotmail.com', '1046714278', 1, 2, '2017-06-16 15:09:44'),
(347, 'R.C.  ', '1043461836', 'MAXILIMIANO ', 'TOVAR SAÑUDO', 'MAIRASALOP@HOTMAIL.COM', '1043461836', 1, 2, '2017-06-16 15:09:44'),
(348, 'NUIP  ', '1043690622', 'CAMILA ANDREA', 'ALTAMIRANDA PERTUZ', 'AUDISPERTUZ@HOTMAIL.CO', '1043690622', 1, 2, '2017-06-16 15:09:44'),
(349, 'NUIP  ', '1041697503', 'EDIER ANDREZ', 'ARISTIZABAL MARQUEZ', 'xxxxx@hotmail.com', '1041697503', 1, 2, '2017-06-16 15:09:44'),
(350, 'NUIP  ', '1033979689', 'AXEL STEVAN', 'DURAN RODRIGUEZ', 'jennyfer.rodriguez4902@correo.policia.gov.co', '1033979689', 1, 2, '2017-06-16 15:09:44'),
(351, 'C.C.  ', '1043689694', 'DANIEL ', 'VERGARA DEL VALLE', 'handy.18@hotmail.com', '1043689694', 1, 2, '2017-06-16 15:09:44'),
(352, 'NUIP  ', '1046719001', 'SANTIAGO JOSE', 'DE AGUAS GUTIERREZ', 'MARYTHEQUEEN90@HOTMAIL.COM', '1046719001', 1, 1, '2017-06-16 15:09:44'),
(353, 'NUIP  ', '1041698587', 'KIARA PATRICIA', 'GUTIERREZ BLANCO', 'HERGUTI66@HOTMAIL.COM', '1041698587', 1, 1, '2017-06-16 15:09:44'),
(354, 'NUIP  ', '1046720175', 'THAEL PAOLA', 'MARTINEZ OLIVERA', 'mariavicky_02@hotmail.com', '1046720175', 1, 1, '2017-06-16 15:09:44'),
(355, 'NUIP  ', '1046713513', 'JESUS DAVID', 'NAVAS VILLAREAL', 'villarrealdelahoz22@hotmail.com', '1046713513', 1, 1, '2017-06-16 15:09:45'),
(356, 'NUIP  ', '1043693259', 'ANA LUCIA', 'ORDOÑEZ SANDOVAL', 'x@hotmail.com', '1043693259', 1, 1, '2017-06-16 15:09:45'),
(357, 'NUIP  ', '1043693489', 'SHAYRA MARCELA', 'PEREZ DE LA ROSA', 'CLAUDIADELAROSA0728@HOTMAIL.COM', '1043693489', 1, 1, '2017-06-16 15:09:45'),
(358, 'NUIP  ', '1046719431', 'DARIANA ', 'RAVE RUIZ', 'yamilis2000@hotmail.com', '1046719431', 1, 1, '2017-06-16 15:09:45'),
(359, 'NUIP  ', '1046719120', 'KEILLYS GUADALUPE', 'CERMEÑO BARRIOS', 'kellybarrios1112@hotmail.com', '1046719120', 1, 1, '2017-06-16 15:09:45'),
(360, 'NUIP  ', '1194970827', 'THIAGO ', 'COLMENARES PARRA', 'x@hotmail.com', '1194970827', 1, 1, '2017-06-16 15:09:45'),
(361, 'NUIP  ', '1044652222', 'KENNETH CARLOS', 'RODRIGUEZ RODRIGUEZ', 'LUZ.RODRIGUEZ01@HOTMAIL.COM', '1044652222', 1, 1, '2017-06-16 15:09:45'),
(362, 'NUIP  ', '1048077842', 'SAHIR DAVID', 'DE LA ROSA ROLDAN', 'x@hotmail.com', '1048077842', 1, 1, '2017-06-16 15:09:45'),
(363, 'R.C.  ', '1081926848', 'CRISTIAN DAVID', 'GARCIA FLOREZ', 'xxxxxx@hotmail.com', '1081926848', 1, 1, '2017-06-16 15:09:45'),
(364, 'R.C.  ', '1043464867', 'BRIANNA ', 'GUZMAN IGLESIAS', 'yeraliglesiasc@hotmail.com', '1043464867', 1, 1, '2017-06-16 15:09:45'),
(365, 'NUIP  ', '1043465961', 'DIEGO ANDRÉS', 'LOPEZ BARRIOS', 'x@hotmail.com', '1043465961', 1, 1, '2017-06-16 15:09:45'),
(366, 'NUIP  ', '1194971597', 'ISABELLA ', 'MEJIA DIAZ', 'alejando-2010@hotmail.com', '1194971597', 1, 1, '2017-06-16 15:09:45'),
(367, 'NUIP  ', '1046717470', 'VALERIA ', 'MENDOZA CANTILLO', 'danielmendoza0786@gmail.com', '1046717470', 1, 1, '2017-06-16 15:09:45'),
(368, 'NUIP  ', '1043692944', 'DANIEL DAVID', 'QUINCHARA SARMIENTO', 'x@hotmail.com', '1043692944', 1, 1, '2017-06-16 15:09:45'),
(369, 'NUIP  ', '1047054589', 'HILLARY ', 'RAVE DE AVILA', 'dayanadeavilab@hotmail.com', '1047054589', 1, 1, '2017-06-16 15:09:45'),
(370, 'NUIP  ', '1043464707', 'FREDDY JOSE', 'ROBAYO PEREZ', 'TATO0324@HOTMAIL.ES', '1043464707', 1, 1, '2017-06-16 15:09:45'),
(371, 'NUIP  ', '1043178210', 'STEPHANIA ', 'VILLALOBOS MARIN', 'x@hotmail.com', '1043178210', 1, 1, '2017-06-16 15:09:45'),
(372, 'NUIP  ', '1046719984', 'LUIS EDUARDO', 'VELILLA HERRERA', 'MILISITA1986@HOTMAIL.COM', '1046719984', 1, 1, '2017-06-16 15:09:45'),
(373, 'NUIP  ', '1045718052', 'MORIS ANDRE', 'CARABALLO LOZANO', 'diemon_79@hotmail.com', '1045718052', 1, 3, '2017-06-16 15:09:45'),
(374, 'NUIP  ', '1043458591', 'HASSAN JOSEPH', 'GONZALEZ BUITRAGO', 'jamabacu@hotmail.com', '1043458591', 1, 3, '2017-06-16 15:09:45'),
(375, 'NUIP  ', '1046709011', 'VALENTINA ', 'OSPINA DIAZ', '', '1046709011', 1, 3, '2017-06-16 15:09:45'),
(376, 'R.C.  ', '1043457144', 'ISABELLA SOFIA', 'SALCEDO CERVANTES', '', '1043457144', 1, 3, '2017-06-16 15:09:46'),
(377, 'R.C.  ', '1048077856', 'ARIANNA MARCELA', 'SALCEDO TAPIA', 'BLEY1110@HOTMAIL.COM', '1048077856', 1, 3, '2017-06-16 15:09:46'),
(378, 'NUIP  ', '1046712145', 'YUSLEIDYS MILAGROS', 'ARIAS ALVAREZ', '', '1046712145', 1, 3, '2017-06-16 15:09:46'),
(379, 'R.C.  ', '1041697437', 'GUADALUPE ', 'ARISTIZABAL ROMO', 'XILENA2509@HOTMAIL.COM', '1041697437', 1, 3, '2017-06-16 15:09:46'),
(380, 'NUIP  ', '1044222321', 'LUNA SHADID', 'BERTEL POLO', 'JOSELUISBERTELPOLO@HOTMAIL.COM', '1044222321', 1, 3, '2017-06-16 15:09:46'),
(381, 'R.C.  ', '1194968084', 'MARIA CAMILA', 'BOSSIO SUAREZ', '', '1194968084', 1, 3, '2017-06-16 15:09:46'),
(382, 'R.C.  ', '1044645348', 'CAMILO EDUARDO', 'VELILLA HERRERA', 'MILISITA1986@HOTMAIL.COM', '1044645348', 1, 3, '2017-06-16 15:09:46'),
(383, 'NUIP  ', '1041697588', 'MATIAS DANIEL', 'CASTILLA ARISTIZABAL', 'jandy1906@hotmail.com', '1041697588', 1, 3, '2017-06-16 15:09:46'),
(384, 'NUIP  ', '1043686819', 'ALBA SANDRY', 'DE LEON ARROYO', 'lucia05071@hotmail.com', '1043686819', 1, 3, '2017-06-16 15:09:46'),
(385, 'R.C.  ', '1043162211', 'LUZ MARINA', 'FERNANDEZ VALENZUELA', 'marjoriecastillo344@hotmail.com', '1043162211', 1, 3, '2017-06-16 15:09:46'),
(386, 'R.C.  ', '1046711418', 'ASHLEY MEILLY', 'GALEANO ZAPATA', 'MAYSSA_84@HOTMAIL.COM', '1046711418', 1, 3, '2017-06-16 15:09:46'),
(387, 'R.C.  ', '1041696806', 'SANTIAGO ', 'LARIOS SANTOS', 'ELISA1025@HOTMAIL.ES', '1041696806', 1, 3, '2017-06-16 15:09:46'),
(388, 'R.C.  ', '1194970390', 'JOSE BAU', 'MESA DE LOS REYES', 'carlosalberto1972@hotmail.es', '1194970390', 1, 3, '2017-06-16 15:09:46');
INSERT INTO `users` (`id`, `tdni`, `dni`, `name`, `last_name`, `email`, `pass_word`, `id_user_type`, `id_grades`, `ts`) VALUES
(389, 'R.C.  ', '1046709741', 'SAMUEL DAVID', 'MONTENEGRO OJEDA', 'ivanmadridojeda@outlook.es', '1046709741', 1, 3, '2017-06-16 15:09:46'),
(390, 'NUIP  ', '1043456859', 'MATIAS ALEXIS', 'PADILLA IGLESIAS', 'IRILES@HOTMAIL.COM', '1043456859', 1, 3, '2017-06-16 15:09:46'),
(391, 'NUIP  ', '1043688195', 'TALIANA MARIA', 'RODRIGUEZ NIÑO', '', '1043688195', 1, 3, '2017-06-16 15:09:46'),
(392, 'NUIP  ', '1043687774', 'MARIANA ISABEL', 'VERA RUSSO', 'IMPORTACIONESMOLINOS@YAHOO.ES', '1043687774', 1, 3, '2017-06-16 15:09:46'),
(393, 'NUIP  ', '1194968793', 'THALIANA ', 'TORRES ALVAREZ', 'sandramilena@hotmail.es', '1194968793', 1, 3, '2017-06-16 15:09:46'),
(394, 'R.C.  ', '1042860482', 'CHEILA JOHANA', 'TRUJILLO GONZALEZ', 'SHIRLY1992@HOTMAIL.ES', '1042860482', 1, 3, '2017-06-16 15:09:46'),
(395, 'NUIP  ', '1043688184', 'MATHIAS ', 'WILCHES OBANDO', 'NANYFER@HOTMAIL.ES', '1043688184', 1, 3, '2017-06-16 15:09:46'),
(396, 'NUIP  ', '1101881223', 'JESUS MIGUEL', 'BLANCO DIAZ', 'lblancod83@hotmail.com', '1101881223', 1, 3, '2017-06-16 15:09:46'),
(397, 'NUIP  ', '1069646998', 'LAURA ', 'GARCIA SERNA', 'jduvan.46@hotmail.com', '1069646998', 1, 3, '2017-06-16 15:09:46'),
(398, 'NUIP  ', '1042861036', 'MELANY ', 'OSPINO VELASQUEZ', 'x@hotmail.com', '1042861036', 1, 3, '2017-06-16 15:09:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_type`
--

CREATE TABLE `user_type` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_type`
--

INSERT INTO `user_type` (`id`, `des`, `ts`) VALUES
(1, 'Estudiantes', '2017-03-30 16:11:24'),
(2, 'Padres de familia', '2017-03-30 16:11:24'),
(3, 'Docentes', '2017-03-30 16:11:24'),
(4, 'Administrativos', '2017-03-30 16:11:24'),
(5, 'Prueba', '2017-05-11 16:18:14');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fault`
--
ALTER TABLE `fault`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fault_type` (`id_fault_type`);

--
-- Indices de la tabla `fault_type`
--
ALTER TABLE `fault_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `id_user_type` (`id_user_type`),
  ADD KEY `id_grades` (`id_grades`);

--
-- Indices de la tabla `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `card`
--
ALTER TABLE `card`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `fault`
--
ALTER TABLE `fault`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `fault_type`
--
ALTER TABLE `fault_type`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `period`
--
ALTER TABLE `period`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=399;
--
-- AUTO_INCREMENT de la tabla `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `fault`
--
ALTER TABLE `fault`
  ADD CONSTRAINT `fault_ibfk_1` FOREIGN KEY (`id_fault_type`) REFERENCES `fault_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_user_type`) REFERENCES `user_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_grades`) REFERENCES `grades` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
