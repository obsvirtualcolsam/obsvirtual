+-- phpMyAdmin SQL Dump
Add a comment to this line
+-- version 4.5.1
+-- http://www.phpmyadmin.net
+--
+-- Servidor: 127.0.0.1
+-- Tiempo de generación: 11-05-2017 a las 17:11:17
+-- Versión del servidor: 10.1.16-MariaDB
+-- Versión de PHP: 5.6.24
+
+SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
+SET time_zone = "+00:00";
+
+
+/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
+/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
+/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
+/*!40101 SET NAMES utf8mb4 */;
+
+--
+-- Base de datos: `obsvirtual`
+--
+
+-- --------------------------------------------------------
+
+--
+-- Estructura de tabla para la tabla `card`
+--
+
+CREATE TABLE `card` (
+  `id` int(1) NOT NULL,
+  `des` varchar(250) DEFAULT NULL,
+  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
+) ENGINE=InnoDB DEFAULT CHARSET=latin1;
+
+--
+-- Volcado de datos para la tabla `card`
+--
+
+INSERT INTO `card` (`id`, `des`, `ts`) VALUES
+(1, 'Tarjeta Amarilla', '2017-03-30 15:40:30'),
+(2, 'Tarjeta Roja', '2017-03-30 15:40:30');
+
+-- --------------------------------------------------------
+
+--
+-- Estructura de tabla para la tabla `fault`
+--
+
+CREATE TABLE `fault` (
+  `id` int(2) NOT NULL,
+  `cod` varchar(250) DEFAULT NULL,
+  `des` varchar(250) DEFAULT NULL,
+  `id_fault_type` int(1) DEFAULT NULL,
+  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
+) ENGINE=InnoDB DEFAULT CHARSET=latin1;
+
+--
+-- Volcado de datos para la tabla `fault`
+--
+
+INSERT INTO `fault` (`id`, `cod`, `des`, `id_fault_type`, `ts`) VALUES
+(1, '22.1', 'Los retardos e inasistencia a clases sin justificación.', 1, '2017-04-06 16:17:29'),
+(2, '22.2', 'El no portar el uniforme y su uso de manera inadecuada', 1, '2017-04-06 16:17:29'),
+(3, '22.3', 'Observaciones o llamados de atención por parte de las personas\r\nexternas sobre comportamientos anti-convivenciales observados en los\r\nestudiantes', 1, '2017-04-06 16:17:29'),
+(4, '22.4', 'El incumplimiento con las tareas y trabajos asignados', 1, '2017-04-06 16:17:29'),
+(5, '22.5', 'La charla constante en la clase o en actos comunitarios', 1, '2017-04-06 16:17:29'),
+(6, '22.6', 'Indelicadeza de forma verbal o física hecha a los compañeros,\r\nprofesores, empleados y demás personas de la comunidad educativa', 1, '2017-04-06 16:17:29'),
+(7, '22.7', 'El juego brusco', 1, '2017-04-06 16:17:29'),
+(8, '22.8', 'La perturbación de clases con dichos o hechos inoportunos', 1, '2017-04-06 16:17:29'),
+(9, '22.9', 'Deficiente presentación personal e higiene corporal, cortes inadecuados,\r\ntintes diversos, peinados extraños y demás formas no clásicas', 1, '2017-04-06 16:17:29'),
+(10, '22.10', 'Permanecer fuera de los salones indicados, en horas de clase o al\r\ncambio de éstas; y la no realización del aseo de los mismos', 1, '2017-04-06 16:17:29'),
+(11, '22.11', 'La utilización del tiempo destinado a las actividades académicas en\r\njuegos o en acciones distractoras', 1, '2017-04-06 16:17:29'),
+(12, '22.12', 'Las manifestaciones afectivas excesivas de cariño con sus compañeros\r\nque conlleve a mal entendidos', 1, '2017-04-06 16:17:29'),
+(13, '22.13', 'El desorden en las formaciones', 1, '2017-04-06 16:17:29'),
+(14, '22.14', 'Gritar a los compañeros', 1, '2017-04-06 16:17:29'),
+(15, '22.15', 'Crear comentarios que deterioren la buena imagen de cualquiera de las\r\npersonas que conforman la familia Albertina (Profesores, Estudiantes,\r\nPadres de Familia, Servicios Generales, Administrativos, Directivos)', 1, '2017-04-06 16:17:29'),
+(16, '22.16', 'Las murmuraciones, acusaciones o información falsa contra\r\nCompañeros, Docentes o cualquier miembro de la comunidad', 1, '2017-04-06 16:17:29'),
+(17, '22.17', 'Retener las comunicaciones que envía el Colegio a los acudientes o\r\npadres de familia o viceversa', 1, '2017-04-06 16:17:29'),
+(18, '22.18', 'El uso de objetos dentro del aula de clases tales como radios, revistas,\r\ngrabadoras, celulares, entre otras, que impidan el buen desarrollo de la\r\nclase', 1, '2017-04-06 16:17:29'),
+(19, '22.19', 'Consumir alimentos en horas de clase y actividades académicas', 1, '2017-04-06 16:17:29'),
+(20, '22.20', 'Hacer trazos, grafitis o escritos en los pupitres, ventanas, paredes,\r\nbaños, vehículos, dentro y/o fuera de la Institución', 1, '2017-04-06 16:17:29'),
+(21, '22.21', 'Hacer caso omiso de los llamados de atención por parte de cualquier\r\nmiembro de la institución (Docentes , Directivos y de Servicio\r\nGenerales)', 1, '2017-04-06 16:17:29'),
+(22, '22.22', 'Con la tercera llegada tarde, se llamará al acudiente para que informe\r\nla situación y tomar los correctivos disciplinarios pertinentes', 1, '2017-04-06 16:17:29'),
+(23, '22.23', 'La no información a los padres de familia o acudientes de las citaciones\r\nverbales o escritas', 1, '2017-04-06 16:17:29'),
+(24, '22.24', 'Registrar tres (3) anotaciones disciplinarias en el observador del\r\nalumno', 1, '2017-04-06 16:17:29'),
+(25, '22.25', 'Realizar actos que atenten contra el orden y la seguridad de la\r\ncomunidad educativa dentro y fuera de la Institución', 1, '2017-04-06 16:17:29'),
+(26, '22.26', 'El porte de celulares y cámaras fotográficas dentro de la Institución', 1, '2017-04-06 16:17:29'),
+(27, '23.1', 'La reincidencia en faltas leves', 2, '2017-04-06 16:17:29'),
+(28, '23.2', 'El hurto en todas sus manifestaciones', 2, '2017-04-06 16:17:29'),
+(29, '23.3', 'El fraude, engaño o suplantación en evaluaciones, trabajos y actividades\r\nasignadas', 2, '2017-04-06 16:17:29'),
+(30, '23.4', 'El irrespeto grave a Directivos, Profesores, Compañeros y demás\r\npersonas dentro o fuera del Colegio', 2, '2017-04-06 16:17:29'),
+(31, '23.5', 'Los actos y expresiones de inmoralidad dentro y fuera del Colegio', 2, '2017-04-06 16:17:29'),
+(32, '23.6', 'El porte, uso o suministro de armas de fuego, corto punzantes,\r\nexplosivos u otros elementos que ocasionen heridas y traumatismo y/o\r\ngeneren desorden.', 2, '2017-04-06 16:17:29'),
+(33, '23.7', 'El consumo o distribución de bebidas embriagantes o drogas\r\nalucinógenas dentro o fuera del Colegio', 2, '2017-04-06 16:17:29'),
+(34, '23.8', 'Falsificación de documentos de la Institución', 2, '2017-04-06 16:17:29'),
+(35, '23.9', 'El porte o divulgación de pornografía, cualquiera que sea el medio\r\nempleado', 2, '2017-04-06 16:17:29'),
+(36, '23.10', 'Las actuaciones fuera del Colegio que perjudiquen gravemente la\r\nbuena imagen de las personas y de la Institución', 2, '2017-04-06 16:17:29'),
+(37, '23.11', 'El maltrato o abuso voluntario de los equipos, implementos didácticos\r\ny otros recursos del Colegio', 2, '2017-04-06 16:17:29'),
+(38, '23.12', 'La incitación a la violencia y el desorden', 2, '2017-04-06 16:17:29'),
+(39, '23.13', 'Intimidación, intento, acoso o soborno dentro y fuera del Colegio a\r\ncualquier miembro de la comunidad educativa', 2, '2017-04-06 16:17:29'),
+(40, '23.14', 'La práctica de juegos de azar y de dinero', 2, '2017-04-06 16:17:29'),
+(41, '23.15', 'La difamación del buen nombre e imagen de la Institución o de las\r\npersonas de la misma, a utilizar el nombre de estos en actividades sin\r\ndebido conocimiento y autorización', 2, '2017-04-06 16:17:29'),
+(42, '23.16', 'Participar en grupos satánicos, metafísicos (mal intencionado),\r\nespiritismo o brujería', 2, '2017-04-06 16:17:29'),
+(43, '23.17', 'El incumplimiento de cualquiera de sus deberes como estudiantes\r\nregistrados en este Manual de Convivencia', 2, '2017-04-06 16:17:29'),
+(44, '23.18', 'Inducir o coaccionar a algún compañero (a) a consumir sustancias\r\nalucinógenas u otra que afecte su estado físico y mental', 2, '2017-04-06 16:17:29'),
+(45, '23.19', 'Agresión física a cualquier miembro de la institución, especialmente a\r\nsus compañeros estudiantes', 2, '2017-04-06 16:17:29'),
+(46, '23.20', 'Inducir o coaccionar a alguna estudiante que pueda estar en embarazo,\r\na un aborto', 2, '2017-04-06 16:17:29'),
+(47, '23.21', 'Amenazar a cualquier miembro de la Institución, o poner en riesgo la\r\nvida de alguno de ellos', 2, '2017-04-06 16:17:29'),
+(48, '23.22', 'Atentar contra los bienes muebles e inmuebles de toda la comunidad\r\neducativa', 2, '2017-04-06 16:17:29'),
+(49, '23.23', 'Apoyar y ejecutar cualquier actividad que vaya en contra de la sana\r\nconvivencia y de la vida', 2, '2017-04-06 16:17:29'),
+(50, '23.24', 'Sabotear las actividades culturales, académicas y sociales que programe\r\nel Colegio', 2, '2017-04-06 16:17:29'),
+(51, '23.25', 'Los comentarios mal intencionados que permitan la discriminación\r\ncon cualquiera de los miembros de la institución, sean de tipo sexual,\r\ncultural, político, religioso, racial y otros que emanen los Derechos\r\nHumanos y la Constitución Política de Col', 2, '2017-04-06 16:17:29'),
+(52, '23.26', 'Cualquier situación que afecte el buen nombre el Colegio San Alberto Magno, como consecuencia de catos que atentan contra las normas de convivencia social', 2, '2017-04-06 16:17:29'),
+(53, '23.27', 'Realizar, promover o participar, sin la correspondiente autorización\r\nde Rectoría, cualquier género de ventas, rifas y negocios con los\r\nCompañeros, Profesores y otros funcionarios del plantel', 2, '2017-04-06 16:17:29'),
+(54, '23.28', 'Utilizar la Internet o páginas sociales empleando lenguajes\r\ninapropiados en contra de la Institución o compañeros y/o entrar en\r\npáginas no autorizadas', 2, '2017-04-06 16:17:29'),
+(55, '23.29', 'Faltas contra el proceso de aprendizaje: la constante indisciplina en\r\nel salón de clase (charlas, gritos, juegos, lanzar objetos, otros) y/o la\r\nindolencia académica', 2, '2017-04-06 16:17:29'),
+(56, '23.30', 'La inasistencia injustificada a los eventos programados por la\r\nInstitución', 2, '2017-04-06 16:17:29'),
+(57, '23.31', 'El vocabulario soez, los insultos, las afrentas, los sobrenombres, las\r\nburlas o ridiculizaciones con base a defectos, limitaciones, características\r\no actitudes de las personas', 2, '2017-04-06 16:17:29'),
+(58, '23.32', 'Motivar o participar en peleas o enfrenamientos dentro o fuera del\r\nColegio', 2, '2017-04-06 16:17:29'),
+(59, '23.33', 'La inasistencia injustificada a clases encontrándose dentro de la\r\nInstitución', 2, '2017-04-06 16:17:29');
+
+-- --------------------------------------------------------
+
+--
+-- Estructura de tabla para la tabla `fault_type`
+--
+
+CREATE TABLE `fault_type` (
+  `id` int(1) NOT NULL,
+  `des` varchar(250) DEFAULT NULL,
+  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
+) ENGINE=InnoDB DEFAULT CHARSET=latin1;
+
+--
+-- Volcado de datos para la tabla `fault_type`
+--
+
+INSERT INTO `fault_type` (`id`, `des`, `ts`) VALUES
+(1, 'Leves', '2017-03-30 15:48:00'),
+(2, 'Graves', '2017-03-30 15:48:00');
+
+-- --------------------------------------------------------
+
+--
+-- Estructura de tabla para la tabla `grades`
+--
+
+CREATE TABLE `grades` (
+  `id` int(1) NOT NULL,
+  `des` varchar(250) DEFAULT NULL,
+  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
+) ENGINE=InnoDB DEFAULT CHARSET=latin1;
+
+--
+-- Volcado de datos para la tabla `grades`
+--
+
+INSERT INTO `grades` (`id`, `des`, `ts`) VALUES
+(1, 'Pre-Jardín', '2017-03-30 16:00:33'),
+(2, 'Jardín', '2017-03-30 16:00:33'),
+(3, 'Transición', '2017-03-30 16:00:33'),
+(4, 'Primero', '2017-03-30 16:00:33'),
+(5, 'Segundo', '2017-03-30 16:00:33'),
+(6, 'Tercero', '2017-03-30 16:00:33'),
+(7, 'Cuarto', '2017-03-30 16:00:33'),
+(8, 'Quinto', '2017-03-30 16:00:33'),
+(9, 'Sexto', '2017-03-30 16:00:33'),
+(10, 'Séptimo', '2017-03-30 16:00:33'),
+(11, 'Octavo', '2017-03-30 16:00:33'),
+(12, 'Noveno', '2017-03-30 16:00:33'),
+(13, 'Décimo', '2017-03-30 16:00:33'),
+(14, 'Úndecimo', '2017-03-30 16:00:33');
+
+-- --------------------------------------------------------
+
+--
+-- Estructura de tabla para la tabla `period`
+--
+
+CREATE TABLE `period` (
+  `id` int(1) NOT NULL,
+  `des` varchar(250) DEFAULT NULL,
+  `b_date` date DEFAULT NULL,
+  `e_date` date DEFAULT NULL,
+  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
+) ENGINE=InnoDB DEFAULT CHARSET=latin1;
+
+--
+-- Volcado de datos para la tabla `period`
+--
+
+INSERT INTO `period` (`id`, `des`, `b_date`, `e_date`, `ts`) VALUES
+(1, 'Iperiodo', '2017-02-01', '2017-04-07', '2017-03-30 15:17:33'),
+(2, 'IIPeriodo', '2017-04-17', '2017-06-16', '2017-03-30 15:17:09'),
+(3, 'IIIPeriodo', '2017-07-13', '2017-09-15', '2017-03-30 15:17:09'),
+(4, 'IVPeridodo', '2017-09-18', '2017-11-17', '2017-03-30 15:17:09');
+
+-- --------------------------------------------------------
+
+--
+-- Estructura de tabla para la tabla `user_type`
+--
+
+CREATE TABLE `user_type` (
+  `id` int(1) NOT NULL,
+  `des` varchar(250) DEFAULT NULL,
+  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
+) ENGINE=InnoDB DEFAULT CHARSET=latin1;
+
+--
+-- Volcado de datos para la tabla `user_type`
+--
+
+INSERT INTO `user_type` (`id`, `des`, `ts`) VALUES
+(1, 'Estudiantes', '2017-03-30 16:11:24'),
+(2, 'Padres de familia', '2017-03-30 16:11:24'),
+(3, 'Docentes', '2017-03-30 16:11:24'),
+(4, 'Administrativos', '2017-03-30 16:11:24');
+
+--
+-- Índices para tablas volcadas
+--
+
+--
+-- Indices de la tabla `card`
+--
+ALTER TABLE `card`
+  ADD PRIMARY KEY (`id`);
+
+--
+-- Indices de la tabla `fault`
+--
+ALTER TABLE `fault`
+  ADD PRIMARY KEY (`id`),
+  ADD KEY `id_fault_type` (`id_fault_type`);
+
+--
+-- Indices de la tabla `fault_type`
+--
+ALTER TABLE `fault_type`
+  ADD PRIMARY KEY (`id`);
+
+--
+-- Indices de la tabla `grades`
+--
+ALTER TABLE `grades`
+  ADD PRIMARY KEY (`id`);
+
+--
+-- Indices de la tabla `period`
+--
+ALTER TABLE `period`
+  ADD PRIMARY KEY (`id`);
+
+--
+-- Indices de la tabla `user_type`
+--
+ALTER TABLE `user_type`
+  ADD PRIMARY KEY (`id`);
+
+--
+-- AUTO_INCREMENT de las tablas volcadas
+--
+
+--
+-- AUTO_INCREMENT de la tabla `card`
+--
+ALTER TABLE `card`
+  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
+--
+-- AUTO_INCREMENT de la tabla `fault`
+--
+ALTER TABLE `fault`
+  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
+--
+-- AUTO_INCREMENT de la tabla `fault_type`
+--
+ALTER TABLE `fault_type`
+  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
+--
+-- AUTO_INCREMENT de la tabla `grades`
+--
+ALTER TABLE `grades`
+  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
+--
+-- AUTO_INCREMENT de la tabla `period`
+--
+ALTER TABLE `period`
+  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
+--
+-- AUTO_INCREMENT de la tabla `user_type`
+--
+ALTER TABLE `user_type`
+  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
+--
+-- Restricciones para tablas volcadas
+--
+
+--
+-- Filtros para la tabla `fault`
+--
+ALTER TABLE `fault`
+  ADD CONSTRAINT `fault_ibfk_1` FOREIGN KEY (`id_fault_type`) REFERENCES `fault_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
+
+/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
+/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;