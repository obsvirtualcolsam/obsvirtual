-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-05-2017 a las 14:50:22
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `obsvirtual`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `card`
--

CREATE TABLE `card` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `card`
--

INSERT INTO `card` (`id`, `des`, `ts`) VALUES
(1, 'Tarjeta Amarilla', '2017-03-30 15:36:50'),
(2, 'Tarjeta Roja', '2017-03-30 15:36:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fault`
--

CREATE TABLE `fault` (
  `id` int(2) NOT NULL,
  `cod` varchar(250) DEFAULT NULL,
  `des` varchar(250) DEFAULT NULL,
  `id_fault_type` int(1) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fault`
--

INSERT INTO `fault` (`id`, `cod`, `des`, `id_fault_type`, `ts`) VALUES
(1, '22.1', 'Los retardos e inasistencias a clases sin justificación', 1, '2017-04-06 15:15:00'),
(2, '22.2', 'El no portar el uniforme o uso de manera inadecuada', 1, '2017-04-06 15:17:24'),
(3, '22.3', 'Observaciones o llamados de atenciones por parte de las personas externas sobre comportamientos anti-convivenciales observados en los estudiantes', 1, '2017-04-06 15:40:14'),
(4, '22.4', 'El incumplimiento con las tareas y trabajos asignados', 1, '2017-04-06 15:40:14'),
(5, '22.5', 'La charla constante en la clase o en actos comunitarios', 1, '2017-04-06 15:40:14'),
(6, '22.6', 'In delicadeza de forma verbal o física hecha a los compañeros, profesores, empleados y demás personas de la comunidad educativa', 1, '2017-04-06 15:40:14'),
(7, '22.7', 'El juego brusco', 1, '2017-04-06 15:40:14'),
(8, '22.8', 'La perturbación de clases con dichos o hechos inoportunos', 1, '2017-04-06 15:40:14'),
(9, '22.9', 'Deficiente presentación personal e higienecorporal, cortes inadecuados, tintes diversos, peinados extraños y demás formas no clásicas', 1, '2017-04-06 15:40:14'),
(10, '22.10', 'Permanecer fuera de los salones indicados, en horas de clase o al cambio de éstas; y la no realización del aseo de los mismos', 1, '2017-04-06 15:40:14'),
(11, '22.11', 'La utilización del tiempo destinado a las actividades académicas en juegos o en acciones distractoras', 1, '2017-04-06 15:40:14'),
(12, '22.12', 'Las manifestaciones afectivas excesivas de cariño con sus compañerosque conlleve a mal entendidos', 1, '2017-04-06 15:40:14'),
(13, '22.13', 'El desorden en las formaciones', 1, '2017-04-06 15:40:14'),
(14, '22.14', 'Gritar a los compañeros', 1, '2017-04-06 15:40:14'),
(15, '22.15', 'Crear comentarios que deterioren la buena imagen de cualquiera de las personas que conforman la familia Albertina (Profesores, Estudiantes, Padres de Familia, Servicios Generales, Administrativos, Directivos)', 1, '2017-04-06 15:40:14'),
(16, '22.16', 'Las murmuraciones, acusaciones o información falsa contra Compañeros, Docentes o cualquier miembro de la comunidad', 1, '2017-04-06 15:40:14'),
(17, '22.17', 'Retener las comunicaciones que envía el Colegio a los acudientes o padres de familia o viceversa', 1, '2017-04-06 15:40:14'),
(18, '22.18', 'El uso de objetos dentro del aula de clases tales como radios, revistas, grabadoras, celulares, entre otras, que impidan el buen desarrollo de la clase', 1, '2017-04-06 15:40:14'),
(19, '22.19', 'Consumir alimentos en horas de clase y actividades académicas', 1, '2017-04-06 15:40:14'),
(20, '22.20', 'Hacer trazos, grafitis o escritos en los pupitres, ventanas, paredes, baños, vehículos, dentro y/o fuera de la Institución', 1, '2017-04-06 15:40:14'),
(21, '22.21', 'Hacer caso omiso de los llamados de atención por parte de cualquier miembro de la institución (Docentes , Directivos y de Servicio Generales)', 1, '2017-04-06 15:40:14'),
(22, '22.22', 'Con la tercera llegada tarde, se llamará al acudiente para que informe la situación y tomar los correctivos disciplinarios pertinentes', 1, '2017-04-06 15:40:14'),
(23, '22.23', 'La no información a los padres de familia o acudientes de las citacionesverbales o escritas', 1, '2017-04-06 15:40:14'),
(24, '22.24', 'Registrar tres (3) anotaciones disciplinarias en el observador del alumno', 1, '2017-04-06 15:40:14'),
(25, '22.25', 'Realizar actos que atenten contra el orden y la seguridad de la comunidad educativa dentro y fuera de la Institución', 1, '2017-04-06 15:40:14'),
(26, '22.26', 'El porte de celulares y cámaras fotográficas dentro de la Institución', 1, '2017-04-06 15:40:14'),
(27, '23.1', 'La reincidencia en faltas leves', 2, '2017-04-06 15:45:47'),
(28, '23.2', 'El hurto en todas sus manifestaciones', 2, '2017-04-06 15:46:25'),
(29, '23.3', 'El fraude, engaño o suplantación en evaluaciones, trabajos y actividades asignadas', 2, '2017-04-06 16:03:01'),
(30, '23.4', 'El irrespeto grave a Directivos, Profesores, Compañeros y demás personas dentro o fuera del Colegio', 2, '2017-04-06 16:03:01'),
(31, '23.5', 'Los actos y expresiones de inmoralidad dentro y fuera del Colegio', 2, '2017-04-06 16:03:01'),
(32, '23.6', 'El porte, uso o suministro de armas de fuego, corto punzantes, explosivos u otros elementos que ocasionen heridas y traumatismo y/o generen desorden', 2, '2017-04-06 16:03:01'),
(33, '23.7', 'El consumo o distribución de bebidas embriagantes o drogas alucinógenas dentro o fuera del Colegio', 2, '2017-04-06 16:03:01'),
(34, '23.8', 'Falsificación de documentos de la Institución', 2, '2017-04-06 16:03:01'),
(35, '23.9', 'El porte o divulgación de pornografía, cualquiera que sea el medio empleado', 2, '2017-04-06 16:03:01'),
(36, '23.10', 'Las actuaciones fuera del Colegio que perjudiquen gravemente la buena imagen de las personas y de la Institución', 2, '2017-04-06 16:03:01'),
(37, '23.11', 'El maltrato o abuso voluntario de los equipos, implementos didácticos y otros recursos del Colegio', 2, '2017-04-06 16:03:01'),
(38, '23.12', 'La incitación a la violencia y el desorden', 2, '2017-04-06 16:03:01'),
(39, '23.13', 'Intimidación, intento, acoso o soborno dentro y fuera del Colegio a cualquier miembro de la comunidad educativa', 2, '2017-04-06 16:03:01'),
(40, '23.14', 'La práctica de juegos de azar y de dinero', 2, '2017-04-06 16:03:01'),
(41, '23.15', 'La difamación del buen nombre e imagen de la Institución o de las personas de la misma, a utilizar el nombre de estos en actividades sin debido conocimiento y autorización', 2, '2017-04-06 16:03:01'),
(42, '23.16', 'Participar en grupos satánicos, metafísicos (mal intencionado), espiritismo o brujería', 2, '2017-04-06 16:03:01'),
(43, '23.17', 'El incumplimiento de cualquiera de sus deberes como estudiantes registrados en este Manual de Convivencia', 2, '2017-04-06 16:03:01'),
(44, '23.18', 'Inducir o coaccionar a algún compañero (a) a consumir sustancias alucinógenas u otra que afecte su estado físico y mental', 2, '2017-04-06 16:03:01'),
(45, '23.19', 'Agresión física a cualquier miembro de la institución, especialmente a sus compañeros estudiantes', 2, '2017-04-06 16:03:01'),
(46, '23.20', 'Inducir o coaccionar a alguna estudiante que pueda estar en embarazo, a un aborto', 2, '2017-04-06 16:03:01'),
(47, '23.21', 'Amenazar a cualquier miembro de la Institución, o poner en riesgo la vida de alguno de ellos', 2, '2017-04-06 16:03:01'),
(48, '23.22', 'Atentar contra los bienes muebles e inmuebles de toda la comunidad educativa', 2, '2017-04-06 16:03:01'),
(49, '23.23', 'Apoyar y ejecutar cualquier actividad que vaya en contra de la sana convivencia y de la vida', 2, '2017-04-06 16:03:01'),
(50, '23.24', 'Sabotear las actividades culturales, académicas y sociales que programe el Colegio', 2, '2017-04-06 16:03:01'),
(51, '23.25', 'Los comentarios mal intencionados que permitan la discriminación con cualquiera de los miembros de la institución, sean de tipo sexual, cultural, político, religioso, racial y otros que emanen los Derechos Humanos y la Constitución Política de Colomb', 2, '2017-04-06 16:03:01'),
(52, '23.26', 'Cualquier situación que afecte el buen nombre del Colegio San Alberto Magno como consecuencia de actos que atentan contra las normas de convivencia social', 2, '2017-04-06 16:03:01'),
(53, '23.27', 'Realizar, promover o participar, sin la correspondiente autorización de Rectoría, cualquier género de ventas, rifas y negocios con los Compañeros, Profesores y otros funcionarios del plantel', 2, '2017-04-06 16:03:01'),
(54, '23.28', 'Utilizar la Internet o páginas sociales empleando lenguajes inapropiados en contra de la Institución o compañeros y/o entrar en páginas no autorizadas', 2, '2017-04-06 16:03:01'),
(55, '23.29', 'Faltas contra el proceso de aprendizaje: la constante indisciplina en el salón de clase (charlas, gritos, juegos, lanzar objetos, otros) y/o la indolencia académica', 2, '2017-04-06 16:03:01'),
(56, '23.30', 'La inasistencia injustificada a los eventos programados por la Institución', 2, '2017-04-06 16:03:01'),
(57, '23.31', 'El vocabulario soez, los insultos, las afrentas, los sobrenombres, las burlas o ridiculizaciones con base a defectos, limitaciones, características o actitudes de las personas', 2, '2017-04-06 16:03:01'),
(58, '23.32', 'Motivar o participar en peleas o enfrenamientos dentro o fuera del Colegio', 2, '2017-04-06 16:03:01'),
(59, '23.33', 'La inasistencia injustificada a clases encontrándose dentro de la Institución', 2, '2017-04-06 16:03:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fault_type`
--

CREATE TABLE `fault_type` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fault_type`
--

INSERT INTO `fault_type` (`id`, `des`, `ts`) VALUES
(1, 'Falta Leve', '2017-03-30 15:46:20'),
(2, 'Falta Grave', '2017-03-30 15:46:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grade`
--

CREATE TABLE `grade` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grade`
--

INSERT INTO `grade` (`id`, `des`, `ts`) VALUES
(1, 'Pre-Jardin', '2017-03-30 16:23:39'),
(2, 'Jardin', '2017-03-30 16:23:51'),
(3, 'Transicion', '2017-03-30 16:24:11'),
(4, 'Primero', '2017-03-30 16:23:09'),
(5, 'Segundo', '2017-03-30 16:24:22'),
(6, 'Tercero', '2017-03-30 16:24:29'),
(7, 'Cuarto', '2017-03-30 16:24:39'),
(8, 'Quinto', '2017-03-30 16:24:48'),
(9, 'Sexto', '2017-03-30 16:24:55'),
(10, 'Septimo', '2017-03-30 16:25:01'),
(11, 'Octavo', '2017-03-30 16:25:08'),
(12, 'Noveno', '2017-03-30 16:25:16'),
(13, 'Decimo', '2017-03-30 16:25:47'),
(14, 'Undecimo', '2017-03-30 16:25:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `period`
--

CREATE TABLE `period` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `b_date` date DEFAULT NULL,
  `e_date` date DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `period`
--

INSERT INTO `period` (`id`, `des`, `b_date`, `e_date`, `ts`) VALUES
(1, 'I Periodo', '2017-02-01', '2017-04-07', '2017-03-30 14:59:08'),
(2, 'II Periodo', '2017-04-17', '2017-06-16', '2017-03-30 14:59:38'),
(3, 'III Periodo', '2017-07-13', '2017-09-15', '2017-03-30 15:01:54'),
(4, 'IV Periodo', '2017-09-18', '2017-11-17', '2017-03-30 15:02:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `tdni` varchar(250) DEFAULT NULL,
  `dni` int(10) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `pass_word` char(32) DEFAULT NULL,
  `id_user_type` int(1) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_type`
--

CREATE TABLE `user_type` (
  `id` int(1) NOT NULL,
  `des` varchar(250) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_type`
--

INSERT INTO `user_type` (`id`, `des`, `ts`) VALUES
(1, 'Estudiantes', '2017-03-30 16:09:06'),
(2, 'Padre de Familia', '2017-03-30 16:09:29'),
(3, 'Docentes', '2017-03-30 16:10:13'),
(4, 'Administrativos', '2017-03-30 16:10:03');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fault`
--
ALTER TABLE `fault`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fault_type` (`id_fault_type`);

--
-- Indices de la tabla `fault_type`
--
ALTER TABLE `fault_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `id_user_type` (`id_user_type`);

--
-- Indices de la tabla `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `card`
--
ALTER TABLE `card`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `fault`
--
ALTER TABLE `fault`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `fault_type`
--
ALTER TABLE `fault_type`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `period`
--
ALTER TABLE `period`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `fault`
--
ALTER TABLE `fault`
  ADD CONSTRAINT `fault_ibfk_1` FOREIGN KEY (`id_fault_type`) REFERENCES `fault_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_user_type`) REFERENCES `user_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
